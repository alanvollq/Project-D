using Core.Data;
using UnityEngine;
using UnityEngine.Serialization;

namespace Core.Loggers
{
    [CreateAssetMenu(fileName = "Core Log Data", menuName = "Data/Loggers/Core Log Data")]
    public class CoreLogData : ScriptableObject
    {
        [SerializeField] private SOLogData _defaultLogData;
        [SerializeField] private SOLogData _systemLogData;
        [SerializeField] private SOLogData _managersLogData;
        [SerializeField] private SOLogData _pageSystemLogData;
        [SerializeField] private SOLogData _popupSystemLogData;
        [SerializeField] private SOLogData _messageboxSystemLogData;

        private static CoreLogData _instance;


        public static SOLogData DefaultLogData => _instance._defaultLogData;
        public static SOLogData SystemLogData => _instance._systemLogData;
        public static SOLogData ManagersLogData => _instance._managersLogData;
        public static SOLogData PageSystemLogData => _instance._pageSystemLogData;
        public static SOLogData PopupSystemLogData => _instance._popupSystemLogData;
        public static SOLogData MessageboxSystemLogData => _instance._messageboxSystemLogData;


        public void Init()
        {
            _instance = this;
        }
    }
}