﻿using System;

namespace Core.StateMachine.Transitions
{
    public interface ITransitionFromAny<out TStateId>
    {
        public TStateId ToState { get; }
        
        public bool IsAvailable { get; }
    }
}