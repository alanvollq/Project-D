﻿using System;

namespace Core.StateMachine.Transitions
{
    public class Transition<TStateId> : TransitionFromAny<TStateId>, ITransition<TStateId>
    {
        public Transition(TStateId fromState, TStateId toState, Func<bool> condition = null) 
            : base(toState, condition)
        {
            FromState = fromState;
        }

        public TStateId FromState { get; }
    }
}