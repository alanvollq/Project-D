﻿using System;
using Core.StateMachine.States;

namespace Core.StateMachine
{
    public interface IFiniteStateMachine<TStateId, in TTriggerId>
    {
        public event Action StateChanged;
        
        
        public IState<TStateId> PreviousState { get; }
        public IState<TStateId> CurrentState { get; }
        
        
        public IFiniteStateMachine<TStateId, TTriggerId> SetStartState(TStateId stateId);
        public IFiniteStateMachine<TStateId, TTriggerId> AddState(IState<TStateId> state);
        public IFiniteStateMachine<TStateId, TTriggerId> AddState(TStateId stateId, Action onEnter = null, Action onExit = null, Action onExecute = null);
        
        public IFiniteStateMachine<TStateId, TTriggerId> AddTransition(TStateId fromState, TStateId toState, Func<bool> condition = null);
        public IFiniteStateMachine<TStateId, TTriggerId> AddTransition(TStateId toState, Func<bool> condition = null);
        public IFiniteStateMachine<TStateId, TTriggerId> AddTransition(TStateId fromState, TStateId toState, TTriggerId triggerId, Func<bool> condition = null);
        public IFiniteStateMachine<TStateId, TTriggerId> AddTransition(TStateId toState, TTriggerId triggerId, Func<bool> condition = null);

        public void InvokeTrigger(TTriggerId trigger);
        
        public void Enable();
        public void Update();
        public void Disable();
    }
}