﻿namespace Core.StateMachine.States
{
    public abstract class State<TStateId> : IState<TStateId>
    {
        protected State(TStateId stateId)
        {
            Id = stateId;
        }
        
        
        public TStateId Id { get; }
        public virtual bool IsExitAvailable { get; protected set; } = true;


        public void Enter() => OnEnter();
        public void Exit() => OnExit();
        public void Execute() => OnExecute();
        

        #region Virtual Methods

        protected virtual void OnEnter() { }

        protected virtual void OnExit() { }

        protected virtual void OnExecute() { }

        #endregion
    }
}