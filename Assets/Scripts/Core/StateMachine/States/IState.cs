﻿namespace Core.StateMachine.States
{
    public interface IState<out TStateId>
    {
        public TStateId Id { get; }
        public bool IsExitAvailable { get; }
        
        public void Enter();
        public void Exit();
        public void Execute();
    }
}