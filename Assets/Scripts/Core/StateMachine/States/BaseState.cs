﻿using System;

namespace Core.StateMachine.States
{
    public class BaseState<TStateId> : State<TStateId>
    {
        private readonly Action _onEnter;
        private readonly Action _onExit;
        private readonly Action _onExecute;
     
        
        public BaseState(TStateId stateId, Action onEnter, Action onExit, Action onExecute) : base(stateId)
        {
            _onEnter = onEnter;
            _onExit = onExit;
            _onExecute = onExecute;
        }

        protected override void OnEnter()
        {
            _onEnter?.Invoke();
        }

        protected override void OnExit()
        {
            _onExit?.Invoke();
        }

        protected override void OnExecute()
        {
            _onExecute?.Invoke();
        }
    }
}