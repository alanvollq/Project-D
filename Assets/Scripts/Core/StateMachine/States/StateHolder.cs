﻿using System;
using System.Collections.Generic;

namespace Core.StateMachine.States
{
    public class StateHolder<TStateId> : IStateHolder<TStateId>
    {
        private readonly Dictionary<TStateId, IState<TStateId>> _states = new();


        public IState<TStateId> this[TStateId stateId] => _states[stateId];


        public IStateHolder<TStateId> AddState(IState<TStateId> state)
        {
            _states.Add(state.Id, state);
            return this;
        }

        public IStateHolder<TStateId> AddState(TStateId stateId, Action onEnter = null, Action onExit = null,
            Action onExecute = null)
        {
            return  AddState(new BaseState<TStateId>(stateId, onEnter, onExit, onExecute));
        }
    }
}