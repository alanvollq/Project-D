﻿using System;

namespace Core.StateMachine.States
{
    public interface IStateHolder<in TStateId>
    {
        public IStateHolder<TStateId> AddState(IState<TStateId> state);

        public IStateHolder<TStateId> AddState(TStateId stateId, Action onEnter = null, Action onExit = null, Action onExecute = null);
    }
}