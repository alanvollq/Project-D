﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Systems.UI.PopupSystem
{
    [Serializable]
    public class PopupAnimatorsHolder
    {
        [SerializeField] private List<UIAnimator> _showAnimators;
        [SerializeField] private List<UIAnimator> _postShowAnimators;
        [SerializeField] private List<UIAnimator> _hideAnimators;
        [SerializeField] private List<UIAnimator> _prependHideAnimators;


        public List<UIAnimator> ShowAnimators => _showAnimators;
        public List<UIAnimator> PostShowAnimators => _postShowAnimators;
        public List<UIAnimator> HideAnimators => _hideAnimators;
        public List<UIAnimator> PrependHideAnimators => _prependHideAnimators;
    }
}