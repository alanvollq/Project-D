﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Systems.UI.PopupSystem.PopupAnimators
{
    public class DefaultPopupBackFieldShowAnimator : UIAnimator
    {
        [SerializeField] private Image _backFieldImage;
        [SerializeField] private float _duration = 0.3f;
        [SerializeField] private Color _startColor;
        [SerializeField] private Color _endColor;


        public override void SetDefaultState()
        {
            _backFieldImage.color = _startColor;
        }

        public override void PlayAnimation(Sequence sequence)
        {
            sequence
                .Join(_backFieldImage.DOColor(_endColor, _duration))
                ;
        }
    }
}