﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Systems.UI.PopupSystem
{
    public sealed class PopupBackField : MonoBehaviour
    {
        [SerializeField] private Button _button;


        public void Init(UnityAction listener)
        {
            _button.onClick.AddListener(listener);
        }

        public void SetActive(bool value)
        {
            _button.gameObject.SetActive(value);
        }
    }
}