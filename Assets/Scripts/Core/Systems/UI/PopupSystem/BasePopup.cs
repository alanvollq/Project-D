using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.GameContext;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Systems.UI.PopupSystem
{
    public abstract class BasePopup : MonoBehaviour
    {
        [SerializeField] private GameObject _rayCastLocker;
        [SerializeField] private PopupBackField _backField;
        [SerializeField] private GameObject _content;
        [SerializeField] private Button _closeButton;
        [SerializeField] private bool _openingIsSingle;

        [SerializeField] private PopupAnimatorsHolder _popupAnimators;

        private bool _isShowed;
        private Sequence _lastPlayedSequence;


        protected IPopupSystem PopupSystem;
        protected IGameContext GameContext;


        public RectTransform RectTransform { get; private set; }
        public bool OpeningIsSingle => _openingIsSingle;
        public bool IsHidden { get; private set; }
        public bool IsAnimationPlay => _lastPlayedSequence != null && _lastPlayedSequence.IsPlaying();


        public BasePopup Initialization(IPopupSystem popupSystem, IGameContext gameContext)
        {
            PopupSystem = popupSystem;
            GameContext = gameContext;

            _backField.Init(OnCloseButtonDown);
            _closeButton.onClick.AddListener(OnCloseButtonDown);

            RectTransform = GetComponent<RectTransform>();

            gameObject.SetActive(false);
            _rayCastLocker.SetActive(false);
            _content.SetActive(false);
            _backField.SetActive(false);

            OnInitialization();

            gameObject.SetActive(false);

            return this;
        }
        
        private void OnCloseButtonDown()
        {
            PopupSystem.CloseLast();
        }

        public void Open(IUIElementOpenParam openParam)
        {
            PlaySequence(CreateOpenSequence(openParam));
        }

        public void Close()
        {
            PlaySequence(CreateCloseSequence());
        }

        public void ForceClose()
        {
            OnBeforeClose();
            ForceHide();
            gameObject.SetActive(false);
            OnAfterClose();
        }

        public void Show()
        {
            if (IsHidden == false)
                return;

            PlaySequence(CreateShowSequence());
        }

        public void ForceHide()
        {
            IsHidden = true;
            _content.SetActive(false);
            _backField.SetActive(false);
        }

        public void Hide()
        {
            if (IsHidden)
                return;

            PlaySequence(CreateHideSequence());
        }

        #region Virtual Methods

        protected virtual void OnInitialization()
        {
        }

        protected virtual void OnBeforeOpen(IUIElementOpenParam openParam)
        {
        }

        protected virtual void OnAfterOpen(IUIElementOpenParam openParam)
        {
        }

        protected virtual void OnBeforeClose()
        {
        }

        protected virtual void OnAfterClose()
        {
        }

        protected virtual void OnBeforeShow()
        {
        }

        protected virtual void OnAfterShow()
        {
        }

        protected virtual void OnBeforeHide()
        {
        }

        protected virtual void OnAfterHide()
        {
        }

        #endregion

        #region Sequence

        private void PlaySequence(Sequence sequence)
        {
            if (IsAnimationPlay)
                _lastPlayedSequence.Pause();

            if (sequence.playedOnce)
                sequence.Rewind();

            sequence.Play();
            _lastPlayedSequence = sequence;
        }

        private Sequence CreateShowSequence()
        {
            var inShowSequence = DOTween.Sequence();
            var postShowSequence = DOTween.Sequence();

            InitAnimators(_popupAnimators.ShowAnimators, inShowSequence);
            InitAnimators(_popupAnimators.PostShowAnimators, postShowSequence);

            var showSequence = DOTween.Sequence();
            showSequence
                .AppendCallback(() => IsHidden = false)
                .AppendCallback(() => _backField.SetActive(true))
                .AppendCallback(() => _content.SetActive(true))
                .Append(inShowSequence)
                .Append(postShowSequence)
                .SetAutoKill(false)
                ;

            return showSequence;
        }

        private Sequence CreateHideSequence()
        {
            var inHideSequence = DOTween.Sequence();
            var prependHideSequence = DOTween.Sequence();

            InitAnimators(_popupAnimators.HideAnimators, inHideSequence);
            InitAnimators(_popupAnimators.PrependHideAnimators, prependHideSequence);

            var hideSequence = DOTween.Sequence();
            hideSequence
                .AppendCallback(() => IsHidden = true)
                .Append(prependHideSequence)
                .Append(inHideSequence)
                .AppendCallback(() => _content.SetActive(false))
                .AppendCallback(() => _backField.SetActive(false))
                .SetAutoKill(false)
                ;

            return hideSequence;
        }

        private static void InitAnimators(IEnumerable<UIAnimator> animators, Sequence sequence)
        {
            animators
                .Where(animator => animator.IsEnable)
                .ToList()
                .ApplyToAll(animator => animator.SetDefaultState())
                .ApplyToAll(animator => animator.PlayAnimation(sequence));
        }

        private Sequence CreateOpenSequence(IUIElementOpenParam openParam)
        {
            var openSequence = DOTween.Sequence();
            openSequence
                .AppendCallback(() => _rayCastLocker.SetActive(true))
                .AppendCallback(() => OnBeforeOpen(openParam))
                .AppendCallback(() => gameObject.SetActive(true))
                .Append(CreateShowSequence())
                .AppendCallback(() => OnAfterOpen(openParam))
                .AppendCallback(() => _rayCastLocker.SetActive(false))
                .SetAutoKill(false)
                ;

            return openSequence;
        }

        private Sequence CreateCloseSequence()
        {
            var closeSequence = DOTween.Sequence();
            closeSequence
                .AppendCallback(() => _rayCastLocker.SetActive(true))
                .AppendCallback(OnBeforeClose)
                .Append(CreateHideSequence())
                .AppendCallback(() => gameObject.SetActive(false))
                .AppendCallback(() => _rayCastLocker.SetActive(false))
                .AppendCallback(OnAfterClose)
                .SetAutoKill(false)
                ;

            return closeSequence;
        }

        #endregion
    }
}