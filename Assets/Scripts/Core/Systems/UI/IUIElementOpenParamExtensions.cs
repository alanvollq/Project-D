namespace Core.Systems.UI
{
    public static class IUIElementOpenParamExtensions
    {
        public static TParam ConvertTo<TParam>(this IUIElementOpenParam openParam)
        {
            if (openParam is not TParam param)
                throw new System.Exception($"{typeof(TParam)} - ui param is not correct!");

            return param;
        }
    }
}