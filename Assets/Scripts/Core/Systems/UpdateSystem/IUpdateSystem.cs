﻿namespace Core.Systems.UpdateSystem
{
    public interface IUpdateSystem : ISystem
    {
        public void AddUpdatable(IUpdatable updatable);
        public void RemoveUpdatable(IUpdatable updatable);
        public void AddFixedUpdatable(IUpdatable updatable);
        public void RemoveFixedUpdatable(IUpdatable updatable);
    }
}