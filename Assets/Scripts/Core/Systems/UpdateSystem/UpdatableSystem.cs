﻿using System;
using System.Collections.Generic;
using Core.GameContext;
using UnityEngine;

namespace Core.Systems.UpdateSystem
{
    public class UpdatableSystem : MonoBehaviour, IUpdateSystem, IGameContextInitializable
    {
        private readonly List<IUpdatable> _updatable = new();
        private readonly List<IUpdatable> _fixedUpdatable = new();


        private void Update()
        {
            foreach (var updatable in _updatable)
                updatable.Update();
        }

        private void FixedUpdate()
        {
            foreach (var updatable in _fixedUpdatable)
                updatable.Update();
        }

        public void AddUpdatable(IUpdatable updatable)
        {
            _updatable.Add(updatable);
        }
        
        public void RemoveUpdatable(IUpdatable updatable)
        {
            _updatable.Remove(updatable);
        }
        
        public void AddFixedUpdatable(IUpdatable updatable)
        {
            _fixedUpdatable.Add(updatable);
        }

        public void RemoveFixedUpdatable(IUpdatable updatable)
        {
            _fixedUpdatable.Remove(updatable);
        }

        public void Initialization(IGameContext gameContext)
        {
            transform.SetParent(gameContext.RootOwner.CoreRoot);
        }
    }
}