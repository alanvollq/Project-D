﻿using System;

namespace Core.Systems.TimeSystem
{
    public interface ITimeSystem : ISystem
    {
        public event Action Paused;
        public event Action<float> Played;
    
        
        public bool IsPause { get; }

        
        public void Pause();
        public void Play();
    }
}