﻿using System;
using UnityEngine;

namespace Core.Systems.TimeSystem
{
    public class TimeSystem : ITimeSystem
    {
        public event Action Paused;
        public event Action<float> Played;

        
        private bool _isPause;
        private float _pauseTime;

        
        public bool IsPause => _isPause;
        
        
        public void Pause()
        {
            _isPause = true;
            _pauseTime = Time.time;
            Paused?.Invoke();
        }

        public void Play()
        {
            _isPause = false;
            Played?.Invoke(Time.time - _pauseTime);
        }
    }
}