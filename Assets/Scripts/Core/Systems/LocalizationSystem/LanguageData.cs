﻿using UnityEngine;

namespace Core.Systems.LocalizationSystem
{
    [CreateAssetMenu(fileName = "Language Data", menuName = "Data/Localization/Language Data")]
    public class LanguageData : ScriptableObject
    {
        [SerializeField] private string _id;
        [SerializeField] private string _name;
        [SerializeField] private string _shortName;


        public string ID => _id;
        public string Name => _name;
        public string ShortName => _shortName;


        public void Init(string name, string shortName)
        {
            
        }
    }
}