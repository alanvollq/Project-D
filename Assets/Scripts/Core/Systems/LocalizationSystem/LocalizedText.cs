﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Systems.LocalizationSystem
{
    [CreateAssetMenu(fileName = "Localized Text", menuName = "Data/Localization/Localized Text")]
    public class LocalizedText : ScriptableObject
    {
        public event Action OnValueChanged;

        [SerializeField] private string _key;
        [SerializeField, TextArea] private string _nativeValue;
        [SerializeField] private List<LanguageText> _values;

        private string _value;
        private ILocalizationSystem _localizationSystem;


        public string Value => _value;


        public void SwitchLanguage(string languageId)
        {
            var languageText = _values.Find(item => item.LanguageId == languageId);
            _value = languageText is not null ? languageText.Value : _nativeValue;
            OnValueChanged?.Invoke();
        }
    }


    [Serializable]
    public class LanguageText
    {
        [SerializeField] private string _languageId;
        [SerializeField, TextArea] private string _value;


        public LanguageText(string languageId, string value)
        {
            _languageId = languageId;
            _value = value;
        }


        public string LanguageId => _languageId;
        public string Value => _value;
    }
}