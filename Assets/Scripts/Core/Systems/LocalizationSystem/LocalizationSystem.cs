﻿using System;
using System.Collections.Generic;

namespace Core.Systems.LocalizationSystem
{
    public class LocalizationSystem : ILocalizationSystem
    {
        public event Action LanguageChanged;

        private readonly IEnumerable<LocalizedText> _localizedTexts;


        public LocalizationSystem(IEnumerable<LocalizedText> localizedTexts,
            LanguageData systemsDataDefaultLanguageData)
        {
            _localizedTexts = localizedTexts;
            SetLanguage(systemsDataDefaultLanguageData);
        }


        public LanguageData CurrentLanguageData { get; private set; }

        public void SetLanguage(LanguageData language)
        {
            foreach (var localizedText in _localizedTexts)
                localizedText.SwitchLanguage(language.ID);

            CurrentLanguageData = language;
            LanguageChanged?.Invoke();
        }
    }
}