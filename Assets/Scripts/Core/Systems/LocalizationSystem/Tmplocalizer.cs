﻿using System;
using TMPro;
using UnityEngine;

namespace Core.Systems.LocalizationSystem
{
    [RequireComponent(typeof(TMP_Text))]
    public class Tmplocalizer : MonoBehaviour
    {
        [SerializeField] private LocalizedText _localizedText;

        private TMP_Text _tmpText;

        
        private void Awake()
        {
            _tmpText = GetComponent<TMP_Text>();
        }

        private void OnEnable()
        {
            _localizedText.OnValueChanged += SetText;
            SetText();
        }

        private void OnDisable()
        {
            _localizedText.OnValueChanged -= SetText;
        }

        private void SetText()
        {
            _tmpText.text = _localizedText.Value;
        }
    }
}