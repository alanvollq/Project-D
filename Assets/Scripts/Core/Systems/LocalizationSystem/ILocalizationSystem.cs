﻿using System;

namespace Core.Systems.LocalizationSystem
{
    public interface ILocalizationSystem : ISystem
    {
        public event Action LanguageChanged;


        public LanguageData CurrentLanguageData { get; }


        public void SetLanguage(LanguageData languageData);
    }
}