﻿namespace Core.Systems.EventSystem
{
    /// <summary>
    /// Represents the base interface for all events that can be raised through the IEventSystem.
    /// </summary>
    public interface IEvent { }
}