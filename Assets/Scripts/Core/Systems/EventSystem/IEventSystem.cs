﻿using System;

namespace Core.Systems.EventSystem
{
    /// <summary>
    /// Defines the contract for an event system that allows components to subscribe to,
    /// unsubscribe from, and raise events.
    /// </summary>
    public interface IEventSystem : ISystem
    {
        /// <summary>
        /// Subscribes a callback to an event of type <typeparamref name="TEvent"/>.
        /// </summary>
        /// <typeparam name="TEvent">The type of event to subscribe to.</typeparam>
        /// <param name="callback">The callback action to invoke when the event is raised.</param>
        void Subscribe<TEvent>(Action<TEvent> callback) where TEvent : IEvent;

        /// <summary>
        /// Unsubscribes a callback from an event of type <typeparamref name="TEvent"/>.
        /// </summary>
        /// <typeparam name="TEvent">The type of event to unsubscribe from.</typeparam>
        /// <param name="callback">The callback action to remove from the event listeners.</param>
        void Unsubscribe<TEvent>(Action<TEvent> callback) where TEvent : IEvent;

        /// <summary>
        /// Raises an event of type <typeparamref name="TEvent"/>, invoking all subscribed callbacks.
        /// </summary>
        /// <typeparam name="TEvent">The type of event to raise.</typeparam>
        /// <param name="event">The event instance containing the event data.</param>
        void Raise<TEvent>(TEvent @event) where TEvent : IEvent;
    }
}