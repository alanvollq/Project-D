using System.Collections.Generic;
using UnityEngine;

namespace Core.Systems.SoundSystem.SoundTracks
{
    [CreateAssetMenu(fileName = "Random Sound Track", menuName = "Data/Sound/Random Sound Track")]
    public class  RandomSoundTrack : SoundTrack
    {
        [SerializeField] private List<AudioClip> _audioClips;

        
        public override AudioClip AudioClip => _audioClips.Count < 1 ? null : _audioClips[Random.Range(0, _audioClips.Count)];
    }
}