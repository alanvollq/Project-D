﻿using UnityEngine;

namespace Core.Systems.SoundSystem.SoundTracks
{
    public abstract class SoundTrack : ScriptableObject, ISoundTrack
    {
        [SerializeField] private SoundTrackEventChannel _soundTrackEventChannel;
        [SerializeField] private bool _isLoop;


        public abstract AudioClip AudioClip { get; }

        public bool IsLoop => _isLoop;


        public void Play() => _soundTrackEventChannel.RaisePlayEvent(this);
    }
}