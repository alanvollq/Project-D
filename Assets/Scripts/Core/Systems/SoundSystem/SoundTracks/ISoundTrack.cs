﻿using UnityEngine;

namespace Core.Systems.SoundSystem.SoundTracks
{
    public interface ISoundTrack
    {
        public AudioClip AudioClip { get; }
        public  bool IsLoop { get; }


        public void Play();
    }
}