using System;
using Core.Pool;
using UnityEngine;

namespace Core.Systems.AudioSystem
{
    public class SoundSource : MonoBehaviour, IPoolable
    {
        public event Action<SoundSource> FinishedPlaying;

        [SerializeField] private AudioSource _audioSource;

        private bool _prevPlayingState;
        private Transform _followTarget;
        private bool _isFollow;


        private void Awake()
        {
            _audioSource.playOnAwake = false;
        }

        private void Update()
        {
            if (_prevPlayingState && !_audioSource.isPlaying)
            {
                _isFollow = false;
                _followTarget = null;
                FinishedPlaying?.Invoke(this);
            }


            if (_isFollow)
                if (_isFollow && _followTarget != null)
                    transform.position = _followTarget.position;
                else
                {
                    _isFollow = false;
                    _followTarget = null;
                }

            _prevPlayingState = _audioSource.isPlaying;
        }

        public void PlayAudioClip(AudioClip clip, bool hasToLoop, Vector3 position)
        {
            _audioSource.transform.position = position;
            PlayAudioClip(clip, hasToLoop);
        }

        public void PlayAudioClip(AudioClip clip, bool hasToLoop, Transform target)
        {
            _isFollow = true;
            _followTarget = target;
            PlayAudioClip(clip, hasToLoop);
        }

        public void PlayAudioClip(AudioClip clip, bool hasToLoop)
        {
            _audioSource.clip = clip;
            _audioSource.loop = hasToLoop;
            _audioSource.time = 0f;
            _audioSource.Play();
        }

        public void Recycle()
        {
            
        }

        public void Release()
        {
            FinishedPlaying = null;
        }
    }
}