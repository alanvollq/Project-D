﻿using Core.GameContext;
using Core.Pool;
using Core.Systems.AudioSystem;
using Core.Systems.SoundSystem.Sound;
using Core.Systems.SoundSystem.SoundTracks;
using UnityEngine;

namespace Core.Systems.SoundSystem
{
    public class SoundSystem : ISoundSystem, IGameContextInitializable
    {
        private readonly SoundSource _soundSourcePrefab;
        
        private ObjectsPool<SoundSource> _objectsPool;


        public SoundSystem(SoundTrackEventChannel[] soundTrackEventChannels, SoundSource soundSourcePrefab,
            SoundSettingsData soundSettingsData)
        {
            soundSettingsData.Init();
            soundSettingsData.SubscribeSetting();
            _soundSourcePrefab = soundSourcePrefab;

            foreach (var soundTrackEventChannel in soundTrackEventChannels)
            {
                soundTrackEventChannel.RemoveAllListeners();
                soundTrackEventChannel.AddListener(PlaySoundTrack);
            }
        }
        
        public void Initialization(IGameContext gameContext)
        {
            var audioSourcesParent = new GameObject("[ Audio Sources ]").transform;
            audioSourcesParent.SetParent(gameContext.RootOwner.MiscRoot);

            _objectsPool = new ObjectsPool<SoundSource>(audioSourcesParent, audioSourcesParent, _soundSourcePrefab);
        }

        private void PlaySoundTrack(ISoundTrack soundTrack)
        {
            var clip = soundTrack.AudioClip;

            if (clip == null)
                return;

            var source = _objectsPool.Get();
            source.PlayAudioClip(clip, soundTrack.IsLoop);
            source.FinishedPlaying += _objectsPool.Return;
        }
    }
}