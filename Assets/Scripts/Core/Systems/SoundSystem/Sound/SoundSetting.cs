using UnityEngine;
using UnityEngine.Audio;

namespace Core.Systems.SoundSystem.Sound
{
    [CreateAssetMenu(fileName = "Sound Setting", menuName = "Data/Sound/Sound Setting")]
    public class SoundSetting : ScriptableObject
    {
        [SerializeField] private FloatEventChannel _volumeChannel;
        [SerializeField] private AudioMixer _audioMixer;
        [SerializeField] private string _groupName;


        public void Init()
        {
            var volume = LoadVolumeValue(GetKey());
            _volumeChannel.RaisePlayEvent(volume);
        }

        public void SubscribeToVolumeChannel() => _volumeChannel.EventRaised += ChangeAudioVolume;

        public void UnsubscribeToVolumeChannel() => _volumeChannel.EventRaised -= ChangeAudioVolume;

        private void ChangeAudioVolume(float volume)
        {
            _audioMixer.SetFloat(_groupName, volume * 100 - 80);
            SaveVolumeValue(GetKey(), volume);
        }

        private string GetKey() => $"Volume value for {_groupName}";

        private static float LoadVolumeValue(string key) => PlayerPrefs.HasKey(key) ? PlayerPrefs.GetFloat(key) : 0;

        private static void SaveVolumeValue(string key, float value) => PlayerPrefs.SetFloat(key, value);
        
        // For Editor
        public void Reset() => PlayerPrefs.DeleteKey(GetKey());
    }
}