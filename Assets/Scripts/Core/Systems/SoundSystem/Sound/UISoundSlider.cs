using UnityEngine;
using UnityEngine.UI;

namespace Core.Systems.SoundSystem.Sound
{
    [RequireComponent(typeof(Slider))]
    public class UISoundSlider : MonoBehaviour
    {
        [SerializeField] private FloatEventChannel _volumeChannel;


        private Slider _slider;


        private void Awake()
        {
            _slider = GetComponent<Slider>();
            _slider.onValueChanged.AddListener(_volumeChannel.RaisePlayEvent);
        }

        private void OnEnable()
        {
            _slider.SetValueWithoutNotify(_volumeChannel.LastValue);
        }
    }
}