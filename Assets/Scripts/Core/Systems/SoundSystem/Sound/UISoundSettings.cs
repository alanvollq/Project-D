using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Systems.SoundSystem.Sound
{
    [Serializable]
    public class UISoundSettings : MonoBehaviour
    {
        [Header("Sliders")]
        [SerializeField] private Slider _allAudioSlider;
        [SerializeField] private Slider _musicSlider;
        [SerializeField] private Slider _sfxSlider;
        [SerializeField] private Slider _dialogueSlider;
        
        [Header("Event Channels")]
        [SerializeField] private FloatEventChannel _allAudioVolumeChannel;
        [SerializeField] private FloatEventChannel _musicVolumeChannel;
        [SerializeField] private FloatEventChannel _sfxVolumeChannel;
        [SerializeField] private FloatEventChannel _dialogueVolumeChannel;
        

        public void Awake()
        {
            _allAudioSlider.onValueChanged.AddListener(_allAudioVolumeChannel.RaisePlayEvent);
            _musicSlider.onValueChanged.AddListener(_musicVolumeChannel.RaisePlayEvent);
            _sfxSlider.onValueChanged.AddListener(_sfxVolumeChannel.RaisePlayEvent);
            _dialogueSlider.onValueChanged.AddListener(_dialogueVolumeChannel.RaisePlayEvent);
        }
        
        public void OnEnable()
        {
            _allAudioSlider.SetValueWithoutNotify(_allAudioVolumeChannel.LastValue);
            _musicSlider.SetValueWithoutNotify(_musicVolumeChannel.LastValue);
            _sfxSlider.SetValueWithoutNotify(_sfxVolumeChannel.LastValue);
            _dialogueSlider.SetValueWithoutNotify(_dialogueVolumeChannel.LastValue);
        }
    }
}