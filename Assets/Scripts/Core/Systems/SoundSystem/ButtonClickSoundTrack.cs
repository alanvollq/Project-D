﻿using Core.Systems.SoundSystem.SoundTracks;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Systems.SoundSystem
{
    [RequireComponent(typeof(Button))]
    public class ButtonClickSoundTrack : MonoBehaviour
    {
        [SerializeField] private SoundTrack _soundTrack;
        
        private Button _button;


        private void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnButtonDown);
        }

        private void OnButtonDown()
        {
            _soundTrack.Play();
        }
    }
}