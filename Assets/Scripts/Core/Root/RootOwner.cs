using UnityEngine;

namespace Core.Root
{
    public sealed class RootOwner : MonoBehaviour, IRootOwner
    {
        [SerializeField] private Transform _parentRoot;
        [SerializeField] private Transform _gameRoot;
        [SerializeField] private Transform _coreRoot;
        [SerializeField] private Transform _miscRoot;
        [SerializeField] private RectTransform _uiRoot;


        public Transform ParentRoot => _parentRoot;
        public Transform GameRoot => _gameRoot;
        public Transform CoreRoot => _coreRoot;
        public Transform MiscRoot => _miscRoot;
        public RectTransform UIRoot => _uiRoot;
    }
}