﻿using UnityEngine.UI;

namespace Core.Extensions
{
    public static class ScrollRectExtension
    {
        public static void SetContentOnStart(this ScrollRect scrollRect)
        {
            var contentPosition = scrollRect.content.position;
            
            contentPosition.y = scrollRect.vertical? 0 :contentPosition.y;
            contentPosition.x = scrollRect.horizontal? 0 :contentPosition.x;

            scrollRect.content.position = contentPosition;
        }
    }
}