using UnityEngine;

namespace Core.Extensions
{
    public static class RectTransformExtension
    {
        public static void SetDefaultMaxStretch(this RectTransform transform)
        {
            transform.anchorMin = new Vector2(0, 0);
            transform.anchorMax = new Vector2(1, 1);
            transform.offsetMin = new Vector2(0, 0);
            transform.offsetMax = new Vector2(0, 0);
        }
    }
}