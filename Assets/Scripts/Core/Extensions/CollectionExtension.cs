﻿using System;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;

namespace Core.Extensions
{
    public static class CollectionExtension
    {
        public static List<T> ApplyToAll<T>(this List<T> list, Action<T> action)
        {
            foreach (var item in list)
                action.Invoke(item);

            return list;
        }
        
        public static IEnumerable<T> ApplyToAll<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var item in enumerable)
                action.Invoke(item);

            return enumerable;
        }

        public static T GetRandom<T>(this List<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }
        
        public static T GetRandom<T>(this IEnumerable<T> enumerable)
        {
            var list = enumerable.ToList();
            return list.GetRandom();
        }
    }
}