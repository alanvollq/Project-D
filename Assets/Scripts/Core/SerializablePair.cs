using System;
using UnityEngine;

namespace Core
{
    [Serializable]
    public class SerializablePair<TKey, TValue>
    {
        [SerializeField] private TKey _key;
        [SerializeField] private TValue _value;
       
        
        public SerializablePair(TKey key, TValue value)
        {
            _key = key;
            _value = value;
        }

        
        public TKey Key => _key;
        public TValue Value => _value;
    }
}