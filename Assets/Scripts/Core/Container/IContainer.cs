namespace Core.Container
{
    public interface IContainer<in TType>
    {
        public void Add<TItem>(TItem item) where TItem : TType;
        public TItem Get<TItem>() where TItem : TType;
    }
}