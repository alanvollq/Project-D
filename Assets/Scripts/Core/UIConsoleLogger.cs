﻿using Core.Loggers;
using TMPro;
using UnityEngine;

namespace Core
{
    public class UIConsoleLogger : MonoBehaviour
    {
        [SerializeField] private TMP_Text _text;

        
        private void Awake()
        {
            ConsoleLogger.Logged += ConsoleLoggerOnLogged;
        }

        private void OnDestroy()
        {
            ConsoleLogger.Logged -= ConsoleLoggerOnLogged;
        }

        private void ConsoleLoggerOnLogged(string message)
        {
            _text.text = message;
        }
    }
}