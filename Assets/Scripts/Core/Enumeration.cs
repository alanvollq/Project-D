using System;
using System.Collections.Generic;

namespace Core
{
    public abstract class Enumeration<T> : IComparable<Enumeration<T>>
    {
        private static readonly IDictionary<string, T> _valueCache =
            new Dictionary<string, T>(StringComparer.OrdinalIgnoreCase);

        private static readonly IDictionary<T, string> _nameCache = new Dictionary<T, string>();

        public string Name { get; }

        public T Value { get; }

        protected Enumeration(string name, T value)
        {
            Value = value;
            Name = name;
            _valueCache.Add(name, value);
            _nameCache.Add(value, name);
        }

        public static IEnumerable<string> Names => _nameCache.Values;

        public static IEnumerable<T> Values => _valueCache.Values;

        public int CompareTo(Enumeration<T> other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return string.Compare(Name, other.Name, StringComparison.Ordinal);
        }
    }
}