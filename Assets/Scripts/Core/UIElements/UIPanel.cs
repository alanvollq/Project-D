using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UIElements
{
    public class UIPanel : MonoBehaviour
    {
        [SerializeField] private Button _backField;
        [SerializeField] private Button _closeButton;


        public bool IsActive => gameObject.activeSelf;
        
        
        private void Awake()
        {
            _closeButton.onClick.AddListener(ClosePanel);
            
            if (_backField != null)
                _backField.onClick.AddListener(ClosePanel);

            OnAwake();
        }
        
        protected virtual void OnAwake(){}

        public void ClosePanel()
        {
            OnClosePanel();
            SetEnable(false);
        }

        protected virtual void OnClosePanel() { }

        public void OpenPanel()
        {
            OnOpenPanel();
            SetEnable(true);
        }

        protected virtual void OnOpenPanel() { }

        protected void SetEnable(bool value)
        {
            gameObject.SetActive(value);
        }
    }
}