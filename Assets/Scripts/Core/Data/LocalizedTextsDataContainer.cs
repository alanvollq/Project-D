﻿using Core.Systems.LocalizationSystem;
using UnityEngine;

namespace Core.Data
{
    [CreateAssetMenu(fileName = "Localized Texts Container", menuName = "Data/Localization/Localized Texts Container")]
    public class LocalizedTextsDataContainer : DataContainer<LocalizedText> { }
}