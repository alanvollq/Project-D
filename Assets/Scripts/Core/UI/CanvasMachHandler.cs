﻿using UnityEngine;
using UnityEngine.UI;

namespace Core.UI
{
    [RequireComponent(typeof(CanvasScaler))]
    public class CanvasMachHandler : MonoBehaviour
    {
        private CanvasScaler _canvasScaler;


        private void Awake()
        {
            _canvasScaler = GetComponent<CanvasScaler>();
        }

        private void Update()
        {
            var ratio = (float)Screen.width / Screen.height;
            _canvasScaler.matchWidthOrHeight = ratio < 1.77f ? 0 : 1;
        }
    }
}