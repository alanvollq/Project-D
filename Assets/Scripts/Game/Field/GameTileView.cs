﻿using System;
using DG.Tweening;
using Game.Data.Field;
using UnityEngine;

namespace Game.Field
{
    public class GameTileView : MonoBehaviour
    {
        [SerializeField] private Transform _mainTransform;
        [SerializeField] private Transform _shadowTransform;
        [SerializeField] private SpriteRenderer _mainSpriteRenderer;
        [SerializeField] private SpriteRenderer _shadowSpriteRenderer;
        [SerializeField] private SpriteRenderer _specialSpriteRenderer;
        [SerializeField] private SpriteRenderer _backgroundSpriteRenderer;
        [SerializeField] private TileType _commonType;
        [SerializeField] private float _fade;

        private TileType _tileType;
        private Vector2Int _coords;
        private ElementType _elementType;


        public TileType TileType => _tileType;
        public ElementType ElementType => _elementType;
        public Vector2Int Coords => _coords;


        private void Awake()
        {
            _specialSpriteRenderer.enabled = false;
            _backgroundSpriteRenderer.enabled = false;
        }

        private void Update()
        {
            var position = _mainTransform.position;
            position.x = Mathf.RoundToInt(position.x);
            position.y = Mathf.RoundToInt(position.y);
            _shadowTransform.DOMove(position, 0.05f);

            _coords = new Vector2Int(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y));
        }

        public void SetShadowEnable(bool value)
        {
            _shadowSpriteRenderer.enabled = value;
        }

        public void SetType(TileType tileType)
        {
            _tileType = tileType;
            var sprite = tileType.Icon;
            _mainSpriteRenderer.sprite = sprite;
            _shadowSpriteRenderer.sprite = sprite;
        }

        private void SetSprite(Sprite sprite)
        {
            _mainSpriteRenderer.sprite = sprite;
            _shadowSpriteRenderer.sprite = sprite;
        }

        public void SetSpecialElement(ElementType elementType)
        {
            _tileType = _commonType;
            
            _elementType = elementType;
            SetSprite(_elementType.Background);
            _mainSpriteRenderer.sprite = _elementType.Background;
            _shadowSpriteRenderer.sprite = _tileType.Icon;
            _specialSpriteRenderer.sprite = _elementType.Icon;
            _specialSpriteRenderer.enabled = true;
            _backgroundSpriteRenderer.enabled = true;
        }

        public void SetEnableLock(bool value)
        {
            _mainSpriteRenderer.DOFade(value?_fade : 1, 0);
            _backgroundSpriteRenderer.DOFade(value?_fade : 1, 0);
            _specialSpriteRenderer.DOFade(value?_fade : 1, 0);
        }
    }
}