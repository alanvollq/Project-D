﻿using DG.Tweening;
using Game.Data.Field;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Field
{
    public class FieldTileView : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _mainBackground;
        [SerializeField] private SpriteRenderer _elementBackground;
        [SerializeField] private SpriteRenderer _elementIcon;

        private TileType _tileType;
        private Transform _transform;
        private ElementType _elementType;


        public bool IsEmpty => _tileType is null;
        public TileType TileType => _tileType;
        public Vector3 Position => _transform.position;


        private void Awake()
        {
            _transform = transform;
            _mainBackground.DOFade(0, 0);
            _elementBackground.enabled = false;
            _elementIcon.enabled = false;
        }

        public void SetTile(TileType tileType)
        {
            _tileType = tileType;
            _mainBackground.sprite = _tileType.Icon;
            _mainBackground.enabled = true;
            _mainBackground.DOFade(1, .1f);
        }

        public void RemoveTile()
        {
            _tileType = null;
            _elementType = null;
            _mainBackground.DOFade(0, 0);
            _mainBackground.transform.DOScale(.8f, 0);
            
            _mainBackground.enabled = false;
            _elementIcon.enabled = false;
            _elementBackground.enabled = false;
        }

        public void Ok()
        {
            _tileType = null;
            var sequence = DOTween.Sequence();
            var iconTransform = _mainBackground.transform;
            sequence
                .Append(iconTransform.DOScale(1f, 0.2f))
                .Append(iconTransform.DOScale(.1f, 0.1f))
                .AppendCallback(RemoveTile)
                ;

            sequence.Play();
        }

        public void SetSpecialElement(ElementType elementType)
        {
            _elementType = elementType;
            _elementIcon.sprite = _elementType.Icon;
            _elementIcon.enabled = true;
            _mainBackground.sprite = _elementType.Background;
            _elementBackground.enabled = true;
            
            _elementIcon.DOFade(1, .1f);
            _elementBackground.DOFade(1, .1f);
        }
    }
}