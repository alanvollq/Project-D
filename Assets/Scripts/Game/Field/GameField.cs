﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Systems.EventSystem;
using Core.Systems.SoundSystem.SoundTracks;
using Game.Data.Field;
using Game.Events;
using Game.Managers.ScoreManager;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Game.Field
{
    public class GameField
    {
        private readonly IScoreManager _scoreManager;
        private readonly Dictionary<Vector2, FieldTileView> _tiles = new();
        private readonly List<FieldTileView> _needClear = new();
        private readonly List<FieldTileView> _chain = new();

        private readonly Vector2[] _neighbourCoords = new Vector2[]
        {
            new(-1, 0),
            new(1, 0),
            new(0, -1),
            new(0, 1)
        };

        private int _fullRowAndColumnCount;
        private Vector3 _fragmentSavePlace;
        private GameFragmentView _savedFragment;
        private int _comboCount;
        private int _comboStep;
        private readonly IEventSystem _eventSystem;
        private readonly SoundTrack _lineFullSoundTrack;

        public bool HasSavedFragment => _savedFragment is not null;

        public GameField(IScoreManager scoreManager, IEventSystem eventSystem, SoundTrack lineFullSoundTrack)
        {
            _scoreManager = scoreManager;
            _eventSystem = eventSystem;
            _lineFullSoundTrack = lineFullSoundTrack;
        }

        public Vector3 FragmentSavePlace => _fragmentSavePlace;

        public void PlaceTiles(List<GameTileView> gameTileViews, Vector3 position)
        {
            foreach (var gameTileView in gameTileViews)
                SetTile(gameTileView);

            foreach (var fieldTileView in _needClear)
                fieldTileView.Ok();

            var score = gameTileViews.Count;
            Debug.Log($"Row And Column: {_fullRowAndColumnCount}");
            if (_fullRowAndColumnCount > 0)
            {
                _comboCount++;
                score += _scoreManager.CalculateScore(_fullRowAndColumnCount, _comboCount);
                _needClear.Clear();
                _comboStep = 0;
                
                _lineFullSoundTrack.Play();
            }
            else
            {
                _comboStep++;
                if (_comboStep == 3)
                {
                    _comboStep = 0;
                    _comboCount = 0;
                }
            }

            Debug.Log($"Combo x{_comboCount}, Score: {score}, cs: {_comboStep}");
            var fieldIsEmpty = _tiles.Values.All(tile => tile.IsEmpty);
            _eventSystem.Raise(new TilePlacedEvent(score, _fullRowAndColumnCount, _comboCount, fieldIsEmpty, position));
            _fullRowAndColumnCount = 0;
            _scoreManager.AddScore(score);
        }

        private void SetTile(GameTileView gameTileView)
        {
            var coords = gameTileView.Coords;
            var tile = _tiles[coords];
            tile.SetTile(gameTileView.TileType);

            if (gameTileView.ElementType is not null)
                tile.SetSpecialElement(gameTileView.ElementType);

            if (_needClear.Contains(tile))
                return;

            // _chain.Clear();
            // CheckTile(tile, coords);
            // if (_chain.Count > 2)
            //     _needClear.AddRange(_chain);

            var column = new List<FieldTileView>();
            var row = new List<FieldTileView>();
            foreach (var tilesKey in _tiles.Keys)
            {
                if (Math.Abs(tilesKey.y - coords.y) < 0.1f)
                    column.Add(_tiles[tilesKey]);

                if (Math.Abs(tilesKey.x - coords.x) < 0.1f)
                    row.Add(_tiles[tilesKey]);
            }

            var hasColumn = false;
            var hasRow = false;
            if (column.All(item => item.IsEmpty == false))
                foreach (var fieldTileView in
                         column.Where(fieldTileView => _needClear.Contains(fieldTileView) == false))
                {
                    _needClear.Add(fieldTileView);
                    hasColumn = true;
                }

            if (hasColumn)
                _fullRowAndColumnCount++;

            if (row.All(item => item.IsEmpty == false))
                foreach (var fieldTileView in row.Where(fieldTileView => _needClear.Contains(fieldTileView) == false))
                {
                    _needClear.Add(fieldTileView);
                    hasRow = true;
                }

            if (hasRow)
                _fullRowAndColumnCount++;
        }

        private void SetTile(TileType tileType, Vector2 coords)
        {
            var tile = _tiles[coords];
            tile.SetTile(tileType);

            if (_needClear.Contains(tile))
                return;

            // _chain.Clear();
            // CheckTile(tile, coords);
            // if (_chain.Count > 2)
            //     _needClear.AddRange(_chain);

            var column = new List<FieldTileView>();
            var line = new List<FieldTileView>();
            foreach (var tilesKey in _tiles.Keys)
            {
                if (Math.Abs(tilesKey.y - coords.y) < 0.1f)
                    column.Add(_tiles[tilesKey]);

                if (Math.Abs(tilesKey.x - coords.x) < 0.1f)
                    line.Add(_tiles[tilesKey]);
            }

            if (column.All(item => item.IsEmpty == false))
                foreach (var fieldTileView in
                         column.Where(fieldTileView => _needClear.Contains(fieldTileView) == false))
                {
                    _needClear.Add(fieldTileView);
                }

            if (line.All(item => item.IsEmpty == false))
                foreach (var fieldTileView in line.Where(fieldTileView => _needClear.Contains(fieldTileView) == false))
                {
                    _needClear.Add(fieldTileView);
                }
        }

        private void CheckTile(FieldTileView fieldTile, Vector2 coords)
        {
            if (_chain.Contains(fieldTile))
                return;

            _chain.Add(fieldTile);

            foreach (var neighbourCoords in _neighbourCoords)
            {
                var checkCoords = coords + neighbourCoords;
                if (_tiles.TryGetValue(checkCoords, out var tile) == false)
                    continue;

                if (tile.TileType == fieldTile.TileType)
                    CheckTile(tile, checkCoords);
            }
        }

        public void SetEnable(bool value)
        {
            foreach (var fieldTileView in _tiles.Values)
                fieldTileView.gameObject.SetActive(value);
        }

        public void AddTile(FieldTileView tile, Vector2 position)
        {
            _tiles.Add(position, tile);
        }

        public bool HasEmptyTile(Vector2 coords)
        {
            foreach (var key in _tiles.Keys)
            {
                if (Vector2.Distance(key, coords) > 0.1f)
                    continue;

                var tile = _tiles[key];
                return tile.IsEmpty;
            }

            return false;
        }

        public void Clear()
        {
            _tiles.Values.ApplyToAll(tile => tile.RemoveTile());
            _fullRowAndColumnCount = 0;
        }

        public void DestroyMap()
        {
            foreach (var fieldTileView in _tiles)
                Object.Destroy(fieldTileView.Value);

            _tiles.Clear();
        }

        public bool HasPlaceForFragment(GameFragmentView gameFragmentView)
        {
            //TODO: Check Available.
            var emptyTiles = _tiles.Values.Where(tile => tile.IsEmpty).ToList();
            var tileCoords = gameFragmentView.GetCoords().ToList();

            if (emptyTiles.Count < tileCoords.Count)
                return false;

            foreach (var fieldTileView in emptyTiles)
            {
                var hasPlace = true;
                var tilePosition = fieldTileView.Position;
                foreach (var coords in tileCoords)
                {
                    var checkCoords = tilePosition + coords;
                    if (HasEmptyTile(checkCoords))
                        continue;

                    hasPlace = false;
                    break;
                }

                if (hasPlace)
                    return true;
            }

            return false;
        }

        public void SetFragmentSavePlace(Vector3 fragmentSavePlace)
        {
            _fragmentSavePlace = fragmentSavePlace;
        }

        public void AddSaveFragment(GameFragmentView gameFragmentView)
        {
            _savedFragment = gameFragmentView;
            _savedFragment.SetStartPosition(_fragmentSavePlace);
            _savedFragment.Placed += SavedFragmentOnPlaced;
        }

        private void SavedFragmentOnPlaced(GameFragmentView fragment)
        {
            _savedFragment.Placed -= SavedFragmentOnPlaced;
            _savedFragment = null;
        }
    }
}