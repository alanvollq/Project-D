﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Systems.SoundSystem.SoundTracks;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using Game.Data.Field;
using Game.Managers.RotateManager;
using Game.Misc;
using UnityEngine;

namespace Game.Field
{
    public class GameFragmentView : MonoBehaviour
    {
        public event Action<GameFragmentView> Placed;
        public event Action<GameFragmentView> Saved;

        [SerializeField] private DragHandler _dragHandler;
        [SerializeField] private ClickHandler _clickHandler;
        [SerializeField] private List<GameTileView> _gameTileViews;
        [SerializeField] private Vector3 _moveOffset = new(0, 3, 0);
        [SerializeField] private Transform _rootTileTransform;
        [SerializeField] private float _lowScale;
        [SerializeField] private GameObject _rotateIcon;
        [SerializeField] private bool _canRotate;
        [SerializeField] float _longDownDuration;
        [SerializeField] private SoundTrack _rotateSoundTrack;
        [SerializeField] private SoundTrack _placeSoundTrack;
        [SerializeField] private SoundTrack _placeSaveSoundTrack;


        private Transform _transform;
        private Camera _camera;
        private Vector3 _startPosition;
        private GameField _gameField;
        private bool _isAvailable = true;
        private Vector3 _touchOffset;
        private bool _isActive;
        private TweenerCore<Vector3, Vector3, VectorOptions> _moveTween;
        private bool _isRotating;
        private bool _isRotateMode;
        private float _lastDownTime;
        private bool _hasDown;
        private bool _isActionPerformed;
        private IRotateManager _rotateManager;


        public bool IsAvailable => _isAvailable;


        private void Awake()
        {
            _camera = Camera.main;
            _transform = transform;
            _clickHandler.Down += OnDown;
            _clickHandler.Up += OnUp;
            _dragHandler.DragStarted += OnDragStarted;

            SetShadowEnable(false);
            _transform.localScale = new Vector3(_lowScale, _lowScale, 1);
            _rotateIcon.SetActive(false);
        }

        private void OnDragStarted()
        {
            if (_isActionPerformed == false)
                StartMove();
        }

        private bool HasLongDown()
        {
            return _hasDown && Time.time - _lastDownTime >= _longDownDuration;
        }

        private void Update()
        {
            if (HasLongDown() && _isActionPerformed == false)
                StartMove();

            if (_isActive == false)
                return;

            var touchPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
            var position = touchPosition - _touchOffset + _moveOffset;
            MoveTo(position, 0.01f);
            SetShadowEnable(CheckAvailablePlacement());
        }

        public void SetStartPosition(Vector3 position)
        {
            _startPosition = position;
            _transform.position = _startPosition;
        }

        private void OnDown()
        {
            _lastDownTime = Time.time;
            _isActionPerformed = false;
            _hasDown = true;

            if (_isRotateMode == false || _canRotate == false)
                StartMove();
        }

        private void StartMove()
        {
            if (_isAvailable == false)
                return;

            if (_isRotating)
                return;

            _isActionPerformed = true;
            _rotateIcon.SetActive(false);


            var touchPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
            var transformPosition = _transform.position;
            _touchOffset = touchPosition - transformPosition;

            var position = touchPosition - _touchOffset + _moveOffset;
            MoveTo(position, 0.01f);
            SetActive(true);
        }

        private void OnUp()
        {
            _hasDown = false;

            if (_isActionPerformed == false)
            {
                Rotate();
                return;
            }

            var placementIsAvailable = CheckAvailablePlacement();

            if (placementIsAvailable == false)
            {
                if (
                    Vector3.Distance(_transform.position, _gameField.FragmentSavePlace) < 2 &&
                    _gameField.HasSavedFragment == false
                )
                {
                    _gameField.AddSaveFragment(this);
                    _placeSaveSoundTrack.Play();
                    Saved?.Invoke(this);
                }

                MoveTo(_startPosition, 0.01f);
                _rotateIcon.SetActive(_isRotateMode);
            }
            else
            {
                _gameField.PlaceTiles(_gameTileViews, _transform.position);
                _placeSoundTrack.Play();
                Placed?.Invoke(this);
                Destroy(gameObject);
            }

            SetActive(false);
        }

        private void SetActive(bool value)
        {
            _isActive = value;

            if (_isActive == false)
                SetShadowEnable(false);

            _transform.DOScale(value ? 1 : _lowScale, 0.1f);
        }

        private void SetShadowEnable(bool value)
        {
            foreach (var gameTileView in _gameTileViews)
                gameTileView.SetShadowEnable(value);
        }

        private bool CheckAvailablePlacement()
        {
            foreach (var gameTileView in _gameTileViews)
            {
                if (_gameField.HasEmptyTile(gameTileView.Coords) == false)
                    return false;
            }

            return true;
        }

        private void MoveTo(Vector3 position, float duration)
        {
            _moveTween?.Kill();

            position.z = 0;
            _moveTween = _transform.DOMove(position, duration);
        }

        public void Init(GameField gameField, TileType tileType, IRotateManager rotateManager)
        {
            _rotateManager = rotateManager;

            foreach (var gameTileView in _gameTileViews)
                gameTileView.SetType(tileType);

            _gameField = gameField;
        }

        public void SetAvailable(bool value)
        {
            _isAvailable = value;
            foreach (var gameTileView in _gameTileViews)
                gameTileView.SetEnableLock(!_isAvailable);
        }

        public IEnumerable<Vector3> GetCoords()
        {
            var rootPosition = _rootTileTransform.position;
            var coordsList = _gameTileViews.Select(tile => (tile.transform.position - rootPosition) / _lowScale);


            return coordsList;
        }

        public void SetSpecialElement(ElementType elementType)
        {
            var gameTileView = _gameTileViews.GetRandom();
            gameTileView.SetSpecialElement(elementType);
        }

        public void ToggleRotateMode()
        {
            if (_canRotate == false)
                return;

            if (_isRotateMode == false && _rotateManager.HasRotate == false)
                return;

            _isRotateMode = !_isRotateMode;
            _rotateIcon.SetActive(_isRotateMode);
        }

        private void Rotate()
        {
            if (_isRotateMode == false)
                return;

            if (_isRotating)
                return;

            _isRotating = true;
            _isActionPerformed = true;

            var sequence = DOTween.Sequence();
            SetAvailable(false);
            _rotateSoundTrack.Play();

            sequence
                .Join(_transform.DORotate(new Vector3(0, 0, transform.eulerAngles.z - 90), 0.2f))
                .AppendCallback(() => _isRotating = false)
                .OnComplete(CheckAvailablePlace)
                .Play()
                ;
        }

        public void CheckAvailablePlace()
        {
            var hasPlace = _gameField.HasPlaceForFragment(this);
            SetAvailable(hasPlace);
        }
    }
}