﻿using Core.Systems.EventSystem;
using UnityEngine;

namespace Game.Events
{
    public record TilePlacedEvent(
        int Score,
        int FullRowAndColumnCount,
        int ComboCount,
        bool FieldIsEmpty,
        Vector3 Position) : IEvent
    {
        public int Score { get; } = Score;
        public int FullRowAndColumnCount { get; } = FullRowAndColumnCount;
        public int ComboCount { get; } = ComboCount;
        public bool FieldIsEmpty { get; } = FieldIsEmpty;
        public Vector3 Position { get; } = Position;
    }
}