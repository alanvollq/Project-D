﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

namespace Game
{
    public class Ball : MonoBehaviour
    {
        [SerializeField] private float _lifeTime;
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private List<Sprite> _sprites;

        private bool _needDestroy;
        
        private void Awake()
        {
            _spriteRenderer.sprite = _sprites[Random.Range(0, _sprites.Count)];
            _needDestroy = false;
            _spriteRenderer.DOFade(Random.Range(0.1f, 0.4f), .5f);
            var scale = Random.Range(0.1f, 0.6f);
            transform.localScale = new Vector3(scale,scale);
        }

        private void Update()
        {
            if(_needDestroy)
                return;
            
            _lifeTime -= Time.deltaTime;
            if (_lifeTime > 0)
            {
                if (Accelerometer.current is null)
                    return;

                var acceleration = Accelerometer.current.acceleration.ReadValue();
                acceleration.z = 0;
                acceleration.x = Mathf.Clamp(acceleration.x, -0.1f, 0.1f);
                acceleration.y = Mathf.Clamp(acceleration.y, -0.1f, 0.1f);
                // acceleration.x *= Time.deltaTime;
                // acceleration.y *= Time.deltaTime;
                transform.DOMove(transform.position + acceleration, .155f);
                return;
            }

            _needDestroy = true;
            _spriteRenderer.DOFade(0, 0.5f).OnComplete(() => Destroy(gameObject));
        }
    }
}