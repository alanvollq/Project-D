﻿using UnityEngine;

namespace Game.Fragments
{
    [CreateAssetMenu(fileName = "Fragment Data", menuName = "Data/Game/Fragment Data")]
    public class FragmentData : ScriptableObject
    {
        [SerializeField] private int[,] _matrix;
    }
}