using System;
using System.Collections.Generic;
using Core.Data;
using Core.Systems.LocalizationSystem;
using Core.Systems.UI.MessageboxSystem;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using UnityEngine;

namespace Game.Setup
{
    [CreateAssetMenu(fileName = "Systems Data", menuName = "Data/Systems Data")]
    public class SystemsData : ScriptableObject
    {
        [SerializeField] private UIContainer _uiContainer;
        [SerializeField] private LocalizedTextsDataContainer _localizedTextsDataContainer;
        [SerializeField] private LanguageData _defaultLanguageData;

        
        public UIContainer UIContainer => _uiContainer;
        public LocalizedTextsDataContainer LocalizedTextsDataContainer => _localizedTextsDataContainer;
        public LanguageData DefaultLanguageData => _defaultLanguageData;
    }


    [Serializable]
    public class UIContainer
    {
        [SerializeField] private PagesContainer _pagesContainer;
        [SerializeField] private PopupsContainer _popupsContainer;
        [SerializeField] private MessageboxesContainer _messageboxesContainer;


        public IEnumerable<BasePage> Pages => _pagesContainer.Data;
        public IEnumerable<BasePopup> Popups => _popupsContainer.Data;
        public IEnumerable<BaseMessagebox> MessageBoxes => _messageboxesContainer.Data;
    }
}