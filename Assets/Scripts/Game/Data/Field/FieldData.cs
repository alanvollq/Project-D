﻿using Core.Systems.SoundSystem.SoundTracks;
using Game.Field;
using UnityEngine;

namespace Game.Data.Field
{
    [CreateAssetMenu(fileName = "Field Data", menuName = "Data/Game/Field Data")]
    public class FieldData : ScriptableObject
    {
        [SerializeField] private Vector2Int _fieldSize;
        [SerializeField] private FieldTileView _tilePrefab;
        [SerializeField] private TileTypesDataContainer _tileTypesDataContainer;
        [SerializeField] private ElementTypesDataContainer _elementTypesDataContainer;
        [SerializeField] private FragmentViewDataContainer _fragmentViewDataContainer;
        [SerializeField] private SoundTrack _lineFullSoundTrack;


        public Vector2Int FieldSize => _fieldSize;
        public FieldTileView TilePrefab => _tilePrefab;
        public TileTypesDataContainer TileTypesDataContainer => _tileTypesDataContainer;
        public ElementTypesDataContainer ElementTypesDataContainer => _elementTypesDataContainer;
        public FragmentViewDataContainer FragmentViewDataContainer => _fragmentViewDataContainer;
        public SoundTrack LineFullSoundTrack => _lineFullSoundTrack;
    }
}