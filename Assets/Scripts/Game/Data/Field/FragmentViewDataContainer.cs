﻿using Core.Data;
using Game.Field;
using UnityEngine;

namespace Game.Data.Field
{
    [CreateAssetMenu(fileName = "Fragment View Data Container", menuName = "Data/Game/Fragment View Data Container")]
    public class FragmentViewDataContainer : DataContainer<GameFragmentView>
    {
    }
}