﻿using System;
using UnityEngine;

namespace Game.Data.Field
{
    [CreateAssetMenu(fileName = "Fragment Data", menuName = "Data/Game/Fragment Data")]
    public class FragmentData : ScriptableObject
    {
        [SerializeField] private FragmentPart[] _fragmentPartsForward;
        [SerializeField] private FragmentPart[] _fragmentPartsBack;
        [SerializeField] private FragmentPart[] _fragmentPartsRight;
        [SerializeField] private FragmentPart[] _fragmentPartsLeft;
    }


    [Serializable]
    public class FragmentPart
    {
        [SerializeField] private Vector2Int[] _coords;

        
        public Vector2Int[] Coords => _coords;
    }
}