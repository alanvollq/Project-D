﻿using UnityEngine;

namespace Game.Data.Field
{
    [CreateAssetMenu(fileName = "Element Type", menuName = "Data/Game/Tiles/Element Type")]
    public class ElementType : ScriptableObject
    {
        [SerializeField] private Sprite _icon;
        [SerializeField] private Sprite _background;


        public Sprite Icon => _icon;
        public Sprite Background => _background;
    }
}