﻿using Core.Data;
using UnityEngine;

namespace Game.Data.Field
{
    [CreateAssetMenu(fileName = "Element Types Data Container", menuName = "Data/Game/Element Types Data Container")]
    public class ElementTypesDataContainer : DataContainer<ElementType>
    {
    }
}