﻿using UnityEngine;

namespace Game.Data.Field
{
    [CreateAssetMenu(fileName = "Tile Type", menuName = "Data/Game/Tiles/Tile Type")]
    public class TileType : ScriptableObject
    {
        [SerializeField] private Sprite _icon;


        public Sprite Icon => _icon;
    }
}