﻿using Core.Data;
using UnityEngine;

namespace Game.Data.Field
{
    [CreateAssetMenu(fileName = "Tile Types Data Container", menuName = "Data/Game/Tile Types Data Container")]
    public class TileTypesDataContainer : DataContainer<TileType>
    {
    }
}