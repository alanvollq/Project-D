﻿using System;
using Core.Managers;

namespace Game.Managers.ScoreManager
{
    public interface IScoreManager : IManager
    {
        public event Action<int> ScoreChanged;
        public event Action<int> BestScoreChanged;

        
        public int Score { get; }
        public int BestScore { get; }

        public void AddScore(int value);
        public void ResetScore();
        public int CalculateScore(int lineAndColumnCount, int comboCount);
    }
}