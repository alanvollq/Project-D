﻿using System;
using Core.GameContext;
using UnityEngine;

namespace Game.Managers.ScoreManager
{
    public class ScoreManager : IScoreManager, IGameContextInitializable
    {
        public event Action<int> ScoreChanged;
        public event Action<int> BestScoreChanged;

        private int _score;
        private int _bestScore = PlayerPrefs.GetInt("Best Score", 0);

        private const int BaseScore = 10;


        public int Score => _score;
        public int BestScore => _bestScore;


        private void SetScore(int value)
        {
            _score = value;
            ScoreChanged?.Invoke(_score);

            if (_score <= _bestScore)
                return;

            SetBestScore(_score);
        }

        private void SetBestScore(int value)
        {
            _bestScore = value;
            PlayerPrefs.SetInt("Best Score", _bestScore);
            BestScoreChanged?.Invoke(_bestScore);
        }

        public void AddScore(int value)
        {
            if (value < 1)
                return;

            SetScore(_score + value);
        }

        public void ResetScore()
        {
            SetScore(0);
        }

        public void Initialization(IGameContext gameContext)
        {
            var bestScore = PlayerPrefs.GetInt("Best Score", 0);
            SetBestScore(bestScore);
        }

        public int CalculateScore(int lineAndColumnCount, int comboCount)
        {
            Debug.Log($"lineAndColumnCount: {lineAndColumnCount}, comboCount: {comboCount}");
            var lineModifier = lineAndColumnCount == 1
                ? 1
                : (lineAndColumnCount - 1) * lineAndColumnCount;


            return lineModifier * BaseScore * comboCount;
        }
    }
}