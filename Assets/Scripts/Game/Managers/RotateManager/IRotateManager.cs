﻿using System;
using Core.Managers;

namespace Game.Managers.RotateManager
{
    public interface IRotateManager : IManager
    {
        public event Action<int> RotateAmountChanged;
        
        public int RotateAmount { get; }
        public bool HasRotate { get; }


        public void AddRotate(int value);
        public void UseRotate();
    }
}