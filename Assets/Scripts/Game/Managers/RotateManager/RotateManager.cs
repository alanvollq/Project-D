﻿using System;
using UnityEngine;

namespace Game.Managers.RotateManager
{
    public class RotateManager : IRotateManager
    {
        public event Action<int> RotateAmountChanged;

        private const string SaveKey = "Rotate Amount";

        private int _rotateAmount = PlayerPrefs.GetInt(SaveKey, 0);


        public int RotateAmount => _rotateAmount;
        public bool HasRotate => _rotateAmount > 0;


        private void SetRotateAmount(int value)
        {
            _rotateAmount = value;
            PlayerPrefs.SetInt(SaveKey, _rotateAmount);
            RotateAmountChanged?.Invoke(_rotateAmount);
        }

        public void AddRotate(int value)
        {
            if (value < 1)
                return;

            SetRotateAmount(_rotateAmount + value);
        }

        public void UseRotate()
        {
            if(_rotateAmount < 1)
                return;

            SetRotateAmount(_rotateAmount - 1);
        }
    }
}