﻿using Core.GameContext;
using Core.Systems.UI.PageSystem;
using Game.Managers.FieldManager;
using Game.Managers.ScoreManager;
using Game.UI.Pages;

namespace Game.Managers.GameStateManager
{
    public class GameStateManager : IGameStateManager, IGameContextInitializable
    {
        private IPageSystem _pageSystem;
        private IFieldManager _fieldManager;
        private IScoreManager _scoreManager;

        public void Initialization(IGameContext gameContext)
        {
            _pageSystem = gameContext.GetSystem<IPageSystem>();
            _fieldManager = gameContext.GetManager<IFieldManager>();
            _scoreManager = gameContext.GetManager<IScoreManager>();
        }

        public void StartGame()
        {
            _pageSystem.Open<MainPage>();
        }

        public void StartLevel()
        {
            _fieldManager.Start();
            _scoreManager.ResetScore();
        }

        public void RestartLevel()
        {
            _fieldManager.End();
            _fieldManager.Start();
            _scoreManager.ResetScore();
        }

        public void OpenMenu()
        {
        }

        public void OpenStore()
        {
            _pageSystem.Open<StorePage>();
        }

        public void StartClassicMode()
        {
            
        }

        public void StartAdventureMode()
        {
            
        }

        public void EndLevel()
        {
            _fieldManager.End();
        }
    }
}