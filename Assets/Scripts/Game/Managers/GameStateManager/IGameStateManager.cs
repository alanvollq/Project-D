﻿using Core.Managers;

namespace Game.Managers.GameStateManager
{
    public interface IGameStateManager : IManager
    {
        public void StartGame();
        public void StartLevel();
        public void RestartLevel();
        public void OpenMenu();
        public void OpenStore();
        public void StartClassicMode();
        public void StartAdventureMode();
        public void EndLevel();
    }
}