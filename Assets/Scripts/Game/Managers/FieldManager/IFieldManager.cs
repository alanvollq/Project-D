﻿using System.Collections.Generic;
using Core.Managers;
using UnityEngine;

namespace Game.Managers.FieldManager
{
    public interface IFieldManager : IManager
    {
        public Vector2 FieldMidPosition { get; }


        public void ToggleRotateMode();
        public void End();
        public void Start();

        public void SetCameraPosition(float calculateOffset);
        public void SetPositions(IEnumerable<Vector3> fragmentsPositions, Vector3 savePlacePosition);

    }
}