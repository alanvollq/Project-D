﻿using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.GameContext;
using Core.Systems.EventSystem;
using Core.Systems.UI.PopupSystem;
using Game.Data.Field;
using Game.Field;
using Game.Managers.RotateManager;
using Game.Managers.ScoreManager;
using Game.UI.Popups;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Game.Managers.FieldManager
{
    public class FieldManager : IFieldManager, IGameContextInitializable
    {
        private Transform _fragmentsParent;
        private Transform _fieldTilesParent;
        private readonly FieldData _fieldData;

        private readonly List<GameFragmentView> _fragmentViews = new();
        private GameField _gameField;
        private IScoreManager _scoreManager;
        private IPopupSystem _popupSystem;
        private float _offset;
        private Vector2 _space;
        private readonly Camera _camera;
        private Vector2Int _fieldSize;
        private bool _hasNewScore;
        private Vector3 _savePlacePosition;
        private List<Vector3> _startPositions;
        private Vector3 _cameraOffset;
        private IRotateManager _rotateManager;
        private IEventSystem _eventSystem;


        public FieldManager(FieldData fieldData)
        {
            _fieldData = fieldData;
            _fieldSize = _fieldData.FieldSize;
            _camera = Camera.main;
        }


        public Vector2 FieldMidPosition { get; private set; }


        public void Initialization(IGameContext gameContext)
        {
            _scoreManager = gameContext.GetManager<IScoreManager>();
            _popupSystem = gameContext.GetSystem<IPopupSystem>();
            _eventSystem = gameContext.GetSystem<IEventSystem>();
            _rotateManager = gameContext.GetManager<IRotateManager>();

            _fragmentsParent = new GameObject("[ Fragments ]").transform;
            _fieldTilesParent = new GameObject("[ Field Tiles ]").transform;

            _fragmentsParent.SetParent(gameContext.RootOwner.GameRoot);
            _fieldTilesParent.SetParent(gameContext.RootOwner.GameRoot);

            _gameField = new GameField(_scoreManager, _eventSystem, _fieldData.LineFullSoundTrack);
            _scoreManager.BestScoreChanged += OnBestScoreChanged;

            CreateMap();
        }

        private void OnBestScoreChanged(int _)
        {
            _hasNewScore = true;
        }

        private void CreateMap()
        {
            for (var x = 0; x < _fieldSize.x; x++)
            {
                for (var y = 0; y < _fieldSize.y; y++)
                {
                    var tile = Object.Instantiate(_fieldData.TilePrefab, _fieldTilesParent);
                    var tilePosition = new Vector2(x, y);
                    tile.transform.position = tilePosition;
                    _gameField.AddTile(tile, tilePosition);
                }
            }

            _gameField.SetEnable(false);

            FieldMidPosition = new Vector2(_fieldSize.x, _fieldSize.y) / 2f;
        }

        private void SetEnableField(bool value)
        {
            _gameField.SetEnable(value);
        }

        private void CreateFragments()
        {
            var tileTypes = _fieldData.TileTypesDataContainer.Data.ToList();
            var elementTypes = _fieldData.ElementTypesDataContainer.Data.ToList();
            var gameFragmentViewPrefabs = _fieldData.FragmentViewDataContainer.Data.ToArray();


            foreach (var startPosition in _startPositions)
            {
                var prefab = gameFragmentViewPrefabs.GetRandom();
                var fragment = Object.Instantiate(prefab, _fragmentsParent);
                fragment.SetStartPosition(startPosition);

                var tileType = tileTypes.GetRandom();
                fragment.Init(_gameField, tileType,_rotateManager);

                // var random = Random.Range(0, 3);
                // if (random < 1)
                // {
                //     var elementType = elementTypes.GetRandom();
                //     fragment.SetSpecialElement(elementType);
                // }

                fragment.Placed += FragmentOnPlaced;
                fragment.Saved += FragmentOnSaved;

                _fragmentViews.Add(fragment);
            }

            CheckAvailablePlace();
        }

        private void FragmentOnSaved(GameFragmentView obj)
        {
            var minFragmentCount = _gameField.HasSavedFragment ? 1 : 0;
            if (_fragmentViews.Count == minFragmentCount)
                CreateFragments();
        }

        private void FragmentOnPlaced(GameFragmentView fragmentView)
        {
            _fragmentViews.Remove(fragmentView);
            var minFragmentCount = _gameField.HasSavedFragment ? 1 : 0;
            if (_fragmentViews.Count == minFragmentCount)
                CreateFragments();

            CheckAvailablePlace();

            if (_fragmentViews.Count(item => item.IsAvailable) < 1)
            {
                var openParam = new ScorePopupOpenParam(_hasNewScore);
                _popupSystem.Open<ScorePopup>(openParam);
            }
        }

        private void CheckAvailablePlace()
        {
            foreach (var gameFragmentView in _fragmentViews)
                gameFragmentView.CheckAvailablePlace();
        }

        private void ClearAll()
        {
            foreach (var fragmentView in _fragmentViews)
                Object.Destroy(fragmentView.gameObject);

            _fragmentViews.Clear();
            _gameField.Clear();
        }


        public void ToggleRotateMode()
        {
            foreach (var gameFragmentView in _fragmentViews)
                gameFragmentView.ToggleRotateMode();
        }

        public void Start()
        {
            SetEnableField(true);
            CreateFragments();
            CheckAvailablePlace();
            _hasNewScore = false;
        }

        public void SetPositions(IEnumerable<Vector3> fragmentsPositions, Vector3 savePlacePosition
        )
        {
            if(_startPositions is not null)
                return;
            
            _savePlacePosition = savePlacePosition + _cameraOffset;
            _savePlacePosition.z = 0;
            
            _gameField.SetFragmentSavePlace(_savePlacePosition);

            _startPositions =
                fragmentsPositions
                    .Select(position => _cameraOffset + new Vector3(position.x, position.y, 10))
                    .ToList();
        }

        public void SetCameraPosition(float fieldMidPointOffset)
        {
            var xOffset = _fieldSize.x / 2f;
            if (_fieldSize.x % 2 == 0)
                xOffset -= 0.5f;

            _cameraOffset = new Vector3(xOffset, fieldMidPointOffset, - 10);
            _camera.transform.position = _cameraOffset;
        }

        public void End()
        {
            SetEnableField(false);
            ClearAll();
        }
    }
}