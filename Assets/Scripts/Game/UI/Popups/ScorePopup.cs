﻿using System;
using Core.Systems.SoundSystem.SoundTracks;
using Core.Systems.UI;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Game.Managers.GameStateManager;
using Game.Managers.ScoreManager;
using Game.UI.Pages;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups
{
    public class ScorePopup : BasePopup
    {
        [SerializeField] private Button _quitButton;
        [SerializeField] private Button _reviveButton;
        [SerializeField] private Button _retryButton;
        [SerializeField] private Button _playButton;
        [SerializeField] private ScorePanel _mainScorePanel;
        [SerializeField] private ScorePanel _bestScorePanel;
        [SerializeField] private SoundTrack _openSoundTrack;

        private IPageSystem _pageSystem;
        private IGameStateManager _gameStateManager;


        protected override void OnInitialization()
        {
            _pageSystem = GameContext.GetSystem<IPageSystem>();

            var scoreManager = GameContext.GetManager<IScoreManager>();
            _gameStateManager = GameContext.GetManager<IGameStateManager>();

            _mainScorePanel.AddScorePanel(scoreManager);
            _bestScorePanel.AddScorePanel(scoreManager);

            _quitButton.onClick.AddListener(OnQuitButtonDown);
            _reviveButton.onClick.AddListener(OnReviveButtonDown);
            _retryButton.onClick.AddListener(OnRetryButtonDown);
            _playButton.onClick.AddListener(OnPlayButtonDown);
        }

        private void OnQuitButtonDown()
        {
            _pageSystem.Open<MainPage>();
        }

        private void OnReviveButtonDown()
        {
        }

        private void OnRetryButtonDown()
        {
            _gameStateManager.RestartLevel();
            PopupSystem.CloseLast();
        }

        private void OnPlayButtonDown()
        {
            _gameStateManager.RestartLevel();
            PopupSystem.CloseLast();
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            if (openParam is not ScorePopupOpenParam scorePopupOpenParam)
                throw new Exception("Open param is not correct");

            var hasNewScore = scorePopupOpenParam.HasNewScore;
            _mainScorePanel.SetEnable(hasNewScore == false);
            _bestScorePanel.SetEnable(hasNewScore);
        }

        protected override void OnAfterOpen(IUIElementOpenParam openParam)
        {
            _openSoundTrack.Play();
        }
    }
}


public record ScorePopupOpenParam(bool HasNewScore) : IUIElementOpenParam
{
    public bool HasNewScore { get; set; } = HasNewScore;
}


[Serializable]
public class ScorePanel
{
    [SerializeField] private GameObject _gameObject;
    [SerializeField] private TMP_Text _bestScoreText;
    [SerializeField] private TMP_Text _scoreText;

    private IScoreManager _scoreManager;


    public void AddScorePanel(IScoreManager scoreManager)
    {
        _scoreManager = scoreManager;
    }

    public void SetEnable(bool value)
    {
        if (value)
        {
            if (_scoreText is not null)
                _scoreText.text = $"{_scoreManager.Score}";

            if (_bestScoreText is not null)
                _bestScoreText.text = $"{_scoreManager.BestScore}";
        }

        _gameObject.SetActive(value);
    }
}