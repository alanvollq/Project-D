﻿using Core.Systems.UI;
using Core.Systems.UI.PopupSystem;
using Game.Managers.RotateManager;
using TMPro;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;
using UnityEngine.UI;

namespace Game.UI.Popups
{
    public class StorePopup : BasePopup
    {
        [SerializeField] private TMP_Text _rotateCountText;
        [SerializeField] private Button _discordButton;
        [SerializeField] private Button _privatePolicyButton;
        [SerializeField] private Button _buttonFree;
        [SerializeField] private Button _buttonFreeAds;

        private IRotateManager _rotateManager;


        protected override void OnInitialization()
        {
            _rotateManager = GameContext.GetManager<IRotateManager>();
            _rotateManager.RotateAmountChanged += RotateAmountChanged;

            _discordButton.onClick.AddListener(OnDiscordButtonDown);
            _privatePolicyButton.onClick.AddListener(OnPrivatePolicyButtonDown);

            _buttonFree.onClick.AddListener(OnFreeButtonDown);
            _buttonFreeAds.onClick.AddListener(OnFreeAdsButtonDown);
        }

        private void OnFreeButtonDown()
        {
            _rotateManager.AddRotate(1);
        }

        private void OnFreeAdsButtonDown()
        {
            _rotateManager.AddRotate(1);
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            SetRotateText();
        }

        private void RotateAmountChanged(int _)
        {
            SetRotateText();
        }

        private void SetRotateText()
        {
            _rotateCountText.text = $"{_rotateManager.RotateAmount}";
        }

        private void OnDiscordButtonDown()
        {
        }

        private void OnPrivatePolicyButtonDown()
        {
        }

        public void Buy10Rotate()
        {
            Debug.Log("Buy10Rotate");
        }

        public void Buy100Rotate()
        {
            Debug.Log("Buy100Rotate");
        }

        public void Buy1000Rotate()
        {
            Debug.Log("Buy1000Rotate");
        }

        public void BuyNoAds()
        {
            Debug.Log("BuyNoAds");
        }

        public void PurchaseFailed(Product product, PurchaseFailureDescription description)
        {
            Debug.Log($"Purchase Failed: {product.transactionID}, {description.message}");
        }

        public void ProductFetched(Product product)
        {
            Debug.Log($"Product Fetched: {product.transactionID}");
        }
    }
}