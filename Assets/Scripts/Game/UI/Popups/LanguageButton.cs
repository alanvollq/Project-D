﻿using System;
using Core.Systems.LocalizationSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups
{
    public class LanguageButton : MonoBehaviour
    {
        public event Action<LanguageData> Down;

        [SerializeField] private LanguageData _languageData;
        [SerializeField] private Image _image;
        [SerializeField] private Button _button;

        private ILocalizationSystem _localizationSystem;


        private void Awake()
        {
            _button = GetComponent<Button>();
            _image = GetComponent<Image>();

            _button.onClick.AddListener(OnButtonDown);
        }

        private void OnButtonDown()
        {
            Down?.Invoke(_languageData);
        }

        public void Init(ILocalizationSystem localizationSystem)
        {
            _localizationSystem = localizationSystem;
            _localizationSystem.LanguageChanged += OnLanguageChanged;
            OnLanguageChanged();
        }

        private void OnLanguageChanged()
        {
            _image.enabled = _languageData == _localizationSystem.CurrentLanguageData;
        }
    }
}