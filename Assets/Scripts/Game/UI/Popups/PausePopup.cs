﻿using System.Collections.Generic;
using Core.Systems.LocalizationSystem;
using Core.Systems.SoundSystem;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Game.UI.Pages;
using Game.UI.UIComponents;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups
{
    public class PausePopup : BasePopup
    {
        [SerializeField] private DoubleToggle _soundToggle;
        [SerializeField] private DoubleToggle _musicToggle;
        [SerializeField] private Button _buttonReplay;
        [SerializeField] private Button _buttonHome;
        [SerializeField] private LanguageData _ruLanguageData;
        [SerializeField] private LanguageData _engLanguageData;
        [SerializeField] private List<LanguageButton> _languageButtons;
        [SerializeField] private FloatEventChannel _musicVolumeEventChannel;
        [SerializeField] private FloatEventChannel _sfxVolumeEventChannel;

        private IPageSystem _pageSystem;
        private ILocalizationSystem _localizationSystem;


        protected override void OnInitialization()
        {
            _pageSystem = GameContext.GetSystem<IPageSystem>();
            _localizationSystem = GameContext.GetSystem<ILocalizationSystem>();

            _buttonReplay.onClick.AddListener(OnReplayButtonDown);
            _buttonHome.onClick.AddListener(OnHomeButtonDown);

            _soundToggle.SetValue(true);
            _musicToggle.SetValue(true);

            _soundToggle.ValueChanged += OnSoundToggleValueChanged;
            _musicToggle.ValueChanged += OnMusicToggleValueChanged;


            foreach (var languageButton in _languageButtons)
            {
                languageButton.Down += OnLanguageButtonDown;
                languageButton.Init(_localizationSystem);
            }
        }

        private void OnMusicToggleValueChanged(bool value)
        {
            _musicVolumeEventChannel.RaisePlayEvent(value ? 1 : 0);
        }

        private void OnSoundToggleValueChanged(bool value)
        {
            _sfxVolumeEventChannel.RaisePlayEvent(value ? 1 : 0);
        }

        private void OnLanguageButtonDown(LanguageData languageData)
        {
            _localizationSystem.SetLanguage(languageData);
        }

        private void OnHomeButtonDown()
        {
            _pageSystem.Open<MainPage>();
        }

        private void OnReplayButtonDown()
        {
            PopupSystem.CloseLast(true);
        }
    }
}