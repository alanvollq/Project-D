﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Systems.LocalizationSystem;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Game.Managers.GameStateManager;
using Game.UI.Popups;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;


namespace Game.UI.Pages
{
    public class MainPage : BasePage
    {
        [SerializeField] private Button _storeButton;
        [SerializeField] private Button _discordButton;
        [SerializeField] private Button _privatePolicyButton;
        [SerializeField] private Button _settingsButton;
        [SerializeField] private Button _infinityModeButton;
        [SerializeField] private Button _adventireModeButton;
        [SerializeField] private GameObject _ballPrefab;
        [SerializeField] private float _spawnTime;


        private IPopupSystem _popupSystem;
        private readonly List<GameObject> _balls = new();
        private float _lastSpawnTime;
        private float _lastTouchSpawn;
        private Camera _camera;
        private IGameStateManager _gameStateManager;
        private ILocalizationSystem _localizationSystem;

        protected override void OnInitialization()
        {
            _popupSystem = GameContext.GetSystem<IPopupSystem>();
            _localizationSystem = GameContext.GetSystem<ILocalizationSystem>();
            _gameStateManager = GameContext.GetManager<IGameStateManager>();

            _infinityModeButton.onClick.AddListener(OnInfinityModeButtonDown);
            _adventireModeButton.onClick.AddListener(OnAdventureModeButtonDown);
            _discordButton.onClick.AddListener(OnDiscordButtonDown);
            _privatePolicyButton.onClick.AddListener(OnPrivatePolicyButtonDown);
            _settingsButton.onClick.AddListener(OnSettingsButtonDown);
            _storeButton.onClick.AddListener(OnStoreButtonDown);

            _camera = Camera.main;
#if UNITY_EDITOR

#else
            InputSystem.EnableDevice(Accelerometer.current);
#endif


            _localizationSystem.LanguageChanged += OnLanguageChanged;
        }

        private void OnLanguageChanged()
        {
        }

        private void OnDiscordButtonDown()
        {
        }

        private void OnPrivatePolicyButtonDown()
        {
            PageSystem.Open<PrivatePolicyPage>();
        }

        private void OnSettingsButtonDown()
        {
        }

        private void OnStoreButtonDown()
        {
            _popupSystem.Open<StorePopup>();
        }

        private void OnInfinityModeButtonDown()
        {
            PageSystem.Open<GamePage>();
            _gameStateManager.StartLevel();
        }

        private void OnAdventureModeButtonDown()
        {
            PageSystem.Open<SelectLevelPage>();
        }

        private void Update()
        {
            _lastTouchSpawn -= Time.deltaTime;
            if (Input.touchCount > 0)
            {
                if (_lastTouchSpawn > 0)
                    return;

                foreach (var touch in Input.touches)
                {
                    var position = touch.position;
                    var gamePos = _camera.ScreenToWorldPoint(position);
                    gamePos.z = 0;
                    var ballX = Instantiate(_ballPrefab);
                    ballX.transform.position = gamePos;
                    _balls.Add(ballX);
                }

                _lastTouchSpawn = _spawnTime;
            }

            _lastSpawnTime -= Time.deltaTime;
            if (_lastSpawnTime > 0)
                return;

#if UNITY_EDITOR

#else
            var ball = Instantiate(_ballPrefab);
            var acceleration = Accelerometer.current.acceleration.ReadValue();
            acceleration.x *= 5;
            acceleration.y *= 3;
            acceleration.z = 0;
            ball.transform.position = new Vector3(Random.Range(-2, 3), Random.Range(-3, 6)) - acceleration;
            _lastSpawnTime = _spawnTime;
#endif
        }

        protected override void OnBeforeClose()
        {
            _balls
                .Where(ball => ball is not null)
                .ApplyToAll(Destroy)
                ;

            _balls.Clear();
        }
    }
}