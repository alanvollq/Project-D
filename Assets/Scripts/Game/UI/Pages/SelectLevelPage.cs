﻿using Core.Systems.LocalizationSystem;
using Core.Systems.UI;
using Core.Systems.UI.PageSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages
{
    public class SelectLevelPage : BasePage
    {
        [SerializeField] private Button _backButton;
        [SerializeField] private Button _playButton;
        [SerializeField] private TMP_Text _playButtonText;
        [SerializeField] private LocalizedText _playButtonKey;
        
        private ILocalizationSystem _localizationSystem;


        protected override void OnInitialization()
        {
           _localizationSystem =  GameContext.GetSystem<ILocalizationSystem>();
           _localizationSystem.LanguageChanged += SetTexts;
           SetTexts();
           
            _backButton.onClick.AddListener(OnBackButtonDown);
            _playButton.onClick.AddListener(OnPlayButtonDown);
        }

        private void SetTexts()
        {
            _playButtonText.text = $"{_playButtonKey.Value} X";
        }

        private void OnBackButtonDown()
        {
            PageSystem.Open<MainPage>();
        }

        private void OnPlayButtonDown()
        {
        }
    }

    public class SelectLevelPageOpenParam : IUIElementOpenParam
    {
        public SelectLevelPageOpenParam(int currentLevel)
        {
            CurrentLevel = currentLevel;
        }

        public int CurrentLevel { get; }
    }
}