﻿using Core.Systems.UI.PageSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages
{
    public class PrivatePolicyPage : BasePage
    {
        [SerializeField] private Button _backButton;
        
        
        protected override void OnInitialization()
        {
            _backButton.onClick.AddListener(OnBackButtonDown);
        }

        private void OnBackButtonDown()
        {
            PageSystem.Open<MainPage>();
        }
    }
}