﻿using Core.Systems.UI;
using Core.Systems.UI.PageSystem;
using Game.Managers.RotateManager;
using TMPro;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;
using UnityEngine.UI;

namespace Game.UI.Pages
{
    public class StorePage : BasePage
    {
        [SerializeField] private TMP_Text _rotateCountText;
        [SerializeField] private Button _backButton;
        [SerializeField] private Button _discordButton;
        [SerializeField] private Button _privatePolicyButton;

        private IRotateManager _rotateManager;


        protected override void OnInitialization()
        {
            _rotateManager = GameContext.GetManager<IRotateManager>();
            _rotateManager.RotateAmountChanged += RotateAmountChanged;

            _backButton.onClick.AddListener(OnBackButtonDown);
            _discordButton.onClick.AddListener(OnDiscordButtonDown);
            _privatePolicyButton.onClick.AddListener(OnPrivatePolicyButtonDown);
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            SetRotateText();
        }

        private void RotateAmountChanged(int _)
        {
            SetRotateText();
        }

        private void SetRotateText()
        {
            _rotateCountText.text = $"{_rotateManager.RotateAmount}";
        }

        private void OnBackButtonDown()
        {
            PageSystem.Open<MainPage>();
        }

        private void OnDiscordButtonDown()
        {
        }

        private void OnPrivatePolicyButtonDown()
        {
        }

        public void Buy10Rotate()
        {
            Debug.Log("Buy10Rotate");
        }

        public void Buy100Rotate()
        {
            Debug.Log("Buy100Rotate");
        }

        public void Buy1000Rotate()
        {
            Debug.Log("Buy1000Rotate");
        }

        public void BuyNoAds()
        {
            Debug.Log("BuyNoAds");
        }

        public void PurchaseFailed()
        {
            Debug.Log($"Purchase Failed");
        }

        public void ProductFetched()
        {
            Debug.Log($"Product Fetched");
        }
    }
}