﻿using System.Collections.Generic;
using System.Linq;
using Core.Systems.EventSystem;
using Core.Systems.UI;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using DG.Tweening;
using Game.Events;
using Game.Managers.FieldManager;
using Game.Managers.GameStateManager;
using Game.Managers.RotateManager;
using Game.Managers.ScoreManager;
using Game.UI.Popups;
using Game.UI.UIComponents;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages
{
    public class GamePage : BasePage
    {
        [SerializeField] private Button _pauseButton;
        [SerializeField] private TMP_Text _currentScoreText;
        [SerializeField] private TMP_Text _bestScoreText;
        [SerializeField] private TMP_Text _rotateAmountText;
        [SerializeField] private Button _rotateButton;
        [SerializeField] private Button _addRotateButton;
        [SerializeField] private RectTransform _topPoint;
        [SerializeField] private RectTransform _midPoint;
        [SerializeField] private List<RectTransform> _fragmentsPlaces;
        [SerializeField] private RectTransform _fragmentSavePlace;
        [SerializeField] private FloatingText _floatingTextPrefab;
        [SerializeField] private Transform _floatingTextParent;

        private IPopupSystem _popupSystem;
        private IFieldManager _fieldManager;
        private IScoreManager _scoreManager;
        private Camera _camera;
        private IRotateManager _rotateManager;
        private IEventSystem _eventSystem;
        private IGameStateManager _gameStateManager;


        protected override void OnInitialization()
        {
            _camera = Camera.main;
            _popupSystem = GameContext.GetSystem<IPopupSystem>();
            _fieldManager = GameContext.GetManager<IFieldManager>();
            _scoreManager = GameContext.GetManager<IScoreManager>();
            _rotateManager = GameContext.GetManager<IRotateManager>();
            _eventSystem = GameContext.GetSystem<IEventSystem>();
            _gameStateManager = GameContext.GetManager<IGameStateManager>();

            _scoreManager.ScoreChanged += OnScoreChanged;
            _scoreManager.BestScoreChanged += OnBestScoreChanged;
            _rotateManager.RotateAmountChanged += SetRotateAmountText;
            SetRotateAmountText(_rotateManager.RotateAmount);

            _pauseButton.onClick.AddListener(OnPauseButtonDown);
            _rotateButton.onClick.AddListener(OnRotateButtonDown);
            _addRotateButton.onClick.AddListener(OnAddRotateButtonDown);

            _eventSystem.Subscribe<TilePlacedEvent>(OnTilePlaced);
        }

        private void OnTilePlaced(TilePlacedEvent tilePlacedEvent)
        {
            var floatingText = Instantiate(_floatingTextPrefab, _floatingTextParent);
            floatingText.SetEvent(tilePlacedEvent);
        }

        private void OnAddRotateButtonDown()
        {
            _popupSystem.Open<StorePopup>();
        }

        private void SetRotateAmountText(int value)
        {
            _rotateAmountText.text = $"{value}";
        }

        private float CalculateOffset(Vector3 targetPosition)
        {
            var cameraSize = _camera.orthographicSize;

            var topPoint = GetWorldPosition(_topPoint.position);
            var targetPoint = GetWorldPosition(targetPosition);
            var distance = Vector3.Distance(topPoint, targetPoint);

            return cameraSize - distance;
        }

        private Vector3 GetWorldPosition(Vector3 position)
        {
            var screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, position);
            var worldPosition = _camera.ScreenToWorldPoint(new Vector3(screenPoint.x, screenPoint.y));
            worldPosition.z = 0;

            return worldPosition;
        }

        private void OnRotateButtonDown()
        {
            _fieldManager.ToggleRotateMode();
        }

        private void OnScoreChanged(int value)
        {
            _currentScoreText.text = $"{value}";
        }

        private void OnBestScoreChanged(int value)
        {
            _bestScoreText.text = $"{value}";
        }

        private void OnPauseButtonDown()
        {
            _popupSystem.Open<PausePopup>();
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            _fieldManager.SetCameraPosition(CalculateOffset(_midPoint.position));

            var fragmentPositions = _fragmentsPlaces.Select(item => item.position);
            _fieldManager.SetPositions(fragmentPositions, _fragmentSavePlace.position);
        }

        protected override void OnBeforeClose()
        {
            _gameStateManager.EndLevel();
        }
    }
}