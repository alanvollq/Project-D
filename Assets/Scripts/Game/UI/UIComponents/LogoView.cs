﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.UIComponents
{
    public class LogoView : MonoBehaviour
    {
        [SerializeField] private HorizontalLayoutGroup _horizontalLayoutGroup;
        [SerializeField] private float _startSpacing;
        [SerializeField] private float _showDuration;


        private void OnEnable()
        {
            _horizontalLayoutGroup.spacing = _startSpacing;
            DOVirtual.Float(_startSpacing, 0, _showDuration, 
                value => _horizontalLayoutGroup.spacing = value);
        }

        private void OnDisable()
        {
            _horizontalLayoutGroup.spacing = _startSpacing;
        }
    }
}