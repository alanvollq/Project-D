﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.UIComponents
{
    [RequireComponent(typeof(Button))]
    public class DoubleToggle : MonoBehaviour
    {
        public event Action<bool> ValueChanged;

        [SerializeField] private StateView _enableStateView;
        [SerializeField] private StateView _disableStateView;

        private Button _button;
        private bool _isOn;


        public bool IsOn => _isOn;


        private void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnButtonDown);
        }

        private void OnButtonDown()
        {
            Toggle();
        }

        public void SetValue(bool value, bool needNotify = true)
        {
            _isOn = value;

            _enableStateView.SetEnable(_isOn);
            _disableStateView.SetEnable(_isOn == false);

            if (needNotify)
                ValueChanged?.Invoke(_isOn);
        }

        public void Toggle()
        {
            SetValue(!IsOn);
        }

        [Serializable]
        private class StateView
        {
            [SerializeField] private Image _image;
            [SerializeField] private Image _icon;


            public void SetEnable(bool value)
            {
                var fadeValue = value ? 1 : 0;

                if (_image is not null)
                {
                    _image.DOKill();
                    _image.DOFade(fadeValue, 0.1f);
                }

                if (_icon is not null)
                {
                    _icon.DOKill();
                    _icon.DOFade(fadeValue, 0.1f);
                }
            }
        }
    }
}