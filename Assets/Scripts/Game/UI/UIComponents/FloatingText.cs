﻿using DG.Tweening;
using Game.Events;
using TMPro;
using UnityEngine;

namespace Game.UI.UIComponents
{
    public class FloatingText : MonoBehaviour
    {
        [SerializeField] private TMP_Text _comboText;
        [SerializeField] private TMP_Text _scoreText;
        [SerializeField] private TMP_ColorGradient _yellowGradient;
        [SerializeField] private TMP_ColorGradient _blueGradient;

        private Transform _transform;

        private void Awake()
        {
            _transform = transform;

            _comboText.enabled = false;
            _scoreText.enabled = false;
        }

        public void SetEvent(TilePlacedEvent tilePlacedEvent)
        {
            var comboCount = tilePlacedEvent.ComboCount;
            var rowAndColumnCount = tilePlacedEvent.FullRowAndColumnCount;
            Debug.Log($"Combo: {comboCount}, rowAndColumnCount: {rowAndColumnCount}");

            if (rowAndColumnCount < 1)
                return;

            _comboText.colorGradientPreset = comboCount < 2 ? _blueGradient : _yellowGradient;
            _scoreText.colorGradientPreset = comboCount < 2 ? _blueGradient : _yellowGradient;

            _comboText.text = comboCount switch
            {
                1 => rowAndColumnCount switch
                {
                    1 => "",
                    2 => $"Good",
                    3 => $"Great ",
                    _ => $"Excellent"
                },
                2 => "Combo",
                _ => $"Combo x{comboCount - 1}"
            };

            _scoreText.enabled = true;
            _comboText.enabled = true;

            var sequence = DOTween.Sequence();

            var position = tilePlacedEvent.Position;
            position.x = Mathf.Clamp(position.x, 2, 5);
            position.y = Mathf.Clamp(position.y, 1, 7);
            position.z = 0;
            _transform.position = position;
            _transform.localScale = Vector3.zero;
            _scoreText.text = $"+{tilePlacedEvent.Score}";
            _scoreText.enabled = true;
            _comboText.enabled = true;

            sequence
                .Append(_transform.DOScale(1.3f, 0.2f))
                .AppendInterval(1f)
                .Append(_transform.DOScale(1.5f, 0.1f))
                .Append(_scoreText.DOFade(0, .25f))
                .Join(_comboText.DOFade(0, .25f))
                .Join(_transform.DOScale(1, .25f))
                .OnComplete(() => Destroy(gameObject))
                ;

            sequence.Play();
            // var comboCount = tilePlacedEvent.ComboCount;
            // if (comboCount < 1)
            //     return;
            //
            // _comboText.DOFade(1, 0);
            // _scoreText.DOFade(1, 0);
            // transform.DOScale(1.2f, 1);
            //
            // if (comboCount == 2)
            //     _comboText.text = $"Combo";
            //
            // if (comboCount > 2)
            //     _comboText.text = $"Combo {comboCount - 1}";
            //
            // _scoreText.text = $"+{tilePlacedEvent.Score}";
            // transform.position = tilePlacedEvent.Position;
            //
            // transform.DOScale(1f, 3);
            // _comboText.DOFade(0, 5);
            // _scoreText.DOFade(0, 5);
        }
    }
}