using System;
using Core.Loggers;
using Game.Misc.Interfaces;
using UnityEngine;
using UnityEngine.EventSystems;
using static Game.Data.Loggers.GameLogData;

namespace Game.Misc
{
    public sealed class DragHandler : MonoBehaviour, IDraggable, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        public event Action DragStarted;
        public event Action Dragged;
        public event Action DragEnded;


        public void OnDrag(PointerEventData eventData) => Dragged?.Invoke();

        public void OnBeginDrag(PointerEventData eventData)
        {
            DragHandlerLogData.Log($"Begin Drag {name}");
            DragStarted?.Invoke();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            DragHandlerLogData.Log($"End Drag {name}");
            DragEnded?.Invoke();
        }
    }
}