using System;
using Core.Loggers;
using Game.Misc.Interfaces;
using UnityEngine;
using UnityEngine.EventSystems;
using static Game.Data.Loggers.GameLogData;

namespace Game.Misc
{
    public sealed class ClickHandler : MonoBehaviour, IClickable, IAvailable, IPointerDownHandler, IPointerUpHandler
    {
        public event Action<bool> AvailableChanged;
        public event Action Down;
        public event Action Up;


        public bool IsAvailable { get; private set; } = true;


        void IAvailable.SetAvailable(bool value)
        {
            IsAvailable = value;
            AvailableChanged?.Invoke(IsAvailable);
        }

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            if (IsAvailable == false)
                return;

            ClickHandlerLogData.Log($"{name} - Down");
            Down?.Invoke();
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            if (IsAvailable == false)
                return;
            
            ClickHandlerLogData.Log($"{name} - Up");
            Up?.Invoke();
        }
    }
}