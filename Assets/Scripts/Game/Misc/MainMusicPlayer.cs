﻿using System;
using Core.Systems.SoundSystem.SoundTracks;
using UnityEngine;

namespace Game.Misc
{
    public class MainMusicPlayer : MonoBehaviour
    {
        [SerializeField] private SoundTrack _soundTrack;


        private void OnEnable()
        {
            _soundTrack.Play();
        }
    }
}