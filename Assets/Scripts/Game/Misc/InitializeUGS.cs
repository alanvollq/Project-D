﻿using System;
using Unity.Services.Core;
using Unity.Services.Core.Environments;
using UnityEngine;

namespace Game.Misc
{
    public class InitializeUGS : MonoBehaviour
    {
        public string environment = "production";

        async void Awake()
        {
            try
            {
                var options = new InitializationOptions()
                    .SetEnvironmentName(environment);

                await UnityServices.InitializeAsync(options);
            }
            catch (Exception exception)
            {
                // An error occurred during initialization.
            }
        }
    }
}