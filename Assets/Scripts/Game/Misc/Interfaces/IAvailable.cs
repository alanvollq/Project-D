namespace Game.Misc.Interfaces
{
    public interface IAvailable
    {
        public event System.Action<bool> AvailableChanged;


        public bool IsAvailable { get; }


        public void SetAvailable(bool value);
    }
}