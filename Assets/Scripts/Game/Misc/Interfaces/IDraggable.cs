namespace Game.Misc.Interfaces
{
    public interface IDraggable
    {
        public event System.Action DragStarted;
        public event System.Action Dragged;
        public event System.Action DragEnded;
    }
}