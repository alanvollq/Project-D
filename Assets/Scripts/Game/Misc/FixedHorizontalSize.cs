﻿using UnityEngine;

namespace Game.Misc
{
    public class FixedHorizontalSize : MonoBehaviour
    {
        [SerializeField] private float _fieldSize;
        private void Awake()
        {
            var mainCamera = Camera.main;
            const float targetAspect = (float)9 / 16;
            var currentAspect = mainCamera.aspect;
            mainCamera.orthographicSize = _fieldSize * (targetAspect / currentAspect);
        }
    }
}