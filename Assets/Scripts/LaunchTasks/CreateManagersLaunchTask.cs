using Core.GameContext;
using Core.GameLaunch;
using Core.Systems.UI;
using Game.Data.Field;
using Game.Managers.FieldManager;
using Game.Managers.GameStateManager;
using Game.Managers.RotateManager;
using Game.Managers.ScoreManager;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Create Managers", fileName = "Create Managers Launch Task")]
    public class CreateManagersLaunchTask : LaunchTask
    {
        [SerializeField] private FieldData _fieldData;


        public override void Execute(IGameContext gameContext)
        {
            CreateCoreManagers(gameContext);
            CreateGameManagers(gameContext);
        }

        private void CreateCoreManagers(IGameContext gameContext)
        {
            var uiManager = new UISystemManager();

            gameContext.AddSystem<IUISystemManager>(uiManager);
        }

        private void CreateGameManagers(IGameContext gameContext)
        {
            var fieldManager = new FieldManager(_fieldData);
            var scoreManager = new ScoreManager();
            var gameStateManager = new GameStateManager();
            var rotateManager = new RotateManager();

            gameContext
                .AddManager<IFieldManager>(fieldManager)
                .AddManager<IScoreManager>(scoreManager)
                .AddManager<IGameStateManager>(gameStateManager)
                .AddManager<IRotateManager>(rotateManager)
                ;
        }
    }
}