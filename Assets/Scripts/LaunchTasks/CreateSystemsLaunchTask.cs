using Core.GameContext;
using Core.GameLaunch;
using Core.Systems.AudioSystem;
using Core.Systems.EventSystem;
using Core.Systems.LocalizationSystem;
using Core.Systems.SoundSystem;
using Core.Systems.SoundSystem.Sound;
using Core.Systems.TimeSystem;
using Core.Systems.UI.MessageboxSystem;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Core.Systems.UpdateSystem;
using Game.Setup;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Create Systems", fileName = "Create Systems Launch Task")]
    public sealed class CreateSystemsLaunchTask : LaunchTask
    {
        [SerializeField] private SystemsData _systemsData;
        [SerializeField] private SoundSettingsData _soundSettingsData;
        [SerializeField] private SoundSource _soundSourcePrefab;
        [SerializeField] private SoundTrackEventChannel[] _soundTrackEventChannels;


        public override void Execute(IGameContext gameContext)
        {
            CreateCoreSystems(gameContext);
            CreateGameSystems(gameContext);
        }

        private void CreateCoreSystems(IGameContext gameContext)
        {
            var timeSystem = new TimeSystem();
            var updatableSystem = new GameObject("Updatable Manager").AddComponent<UpdatableSystem>();
            var eventSystem = new EventSystem();
            var soundSystem = new SoundSystem(_soundTrackEventChannels,_soundSourcePrefab,_soundSettingsData);
            var localizationSystem = new LocalizationSystem(
                _systemsData.LocalizedTextsDataContainer.Data,
                _systemsData.DefaultLanguageData
                );

            gameContext
                .AddSystem<ITimeSystem>(timeSystem)
                .AddSystem<IUpdateSystem>(updatableSystem)
                .AddSystem<IEventSystem>(eventSystem)
                .AddSystem<ISoundSystem>(soundSystem)
                .AddSystem<ILocalizationSystem>(localizationSystem)
                ;

            CreateUISystems(gameContext);
        }

        private void CreateUISystems(IGameContext gameContext)
        {
            var uiContainer = _systemsData.UIContainer;
            var pageSystem = new PageSystem(uiContainer.Pages);
            var popupSystem = new PopupSystem(uiContainer.Popups);
            var messageboxSystem = new MessageboxSystem(uiContainer.MessageBoxes);

            gameContext
                .AddSystem<IPageSystem>(pageSystem)
                .AddSystem<IPopupSystem>(popupSystem)
                .AddSystem<IMessageboxSystem>(messageboxSystem)
                ;
        }

        private void CreateGameSystems(IGameContext gameContext)
        {
            // var inputSystem = new InputSystem();
            // gameContext
            // .AddSystem<IInputSystem>(inputSystem)
            // ;
        }
    }
}